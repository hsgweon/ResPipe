#!/usr/bin/env/python

############################################################
# Argument Options

import argparse, os, shutil
from argparse import RawTextHelpFormatter

parser = argparse.ArgumentParser("Description: will check all files listed in the list exists in the directory.")

parser.add_argument("--in",
					action = "store",
					dest = "indir",
					metavar = "indir",
					help = "[REQUIRED] Directory where .bam files are.",
					required = True)
parser.add_argument("--fastq",
					action = "store_true",
					dest = "inputIsFastq",
					help = "Use this to specify that the input files are \".fastq.gz\" [by default, the inputs are BAM files.]",
					required = False)
parser.add_argument("-l",
					action = "store",
					dest = "list",
					metavar = "list",
					help = "[REQUIRED] metadata file. Need at least two columns - one with BAM file names without the extension, another with sample IDs",
					required = True)
# parser.add_argument("-o",
# 					action = "store",
# 					dest = "outdir",
# 					metavar = "outdir",
# 					help = "[REQUIRED] Output directory where symlinked BAM files with ID changes will be put into",
# 					required = True)
options = parser.parse_args()

############################################################

infile_list = open(options.list, "r")

filenames = []
sampleids = []

for line in infile_list:
	if line.startswith("#"):
		continue
	line = line.rstrip().split()
	filenames.append(line[0])
	sampleids.append(line[1])

# Check for duplication in the metadatq
offending_filenames = []
offending_sampleids  = []

for s in list(set(filenames)):
	if filenames.count(s) != 1:
		offending_filenames.append(s)

# for s in list(set(sampleids)):
#     if sampleids.count(s) != 1:
#         offending_sampleids.append(s)

if len(offending_filenames):
	print("Error: More than one occurence (filename): " + ",".join(offending_filenames))
	print("Please resolve the filename issues and run the command again.")
	exit(1)

# Check for presence of files listed in the metadata
filenames_missing = []
for i in range(len(filenames)):

	if not options.inputIsFastq:

		filename = os.path.abspath(options.indir) + "/" + filenames[i] + ".bam"
		if not os.path.exists(filename):
			filenames_missing.append(filename)

	else:

		filename = os.path.abspath(options.indir) + "/" + filenames[i] + "_r1.fastq.gz"
		if not os.path.exists(filename):
			filenames_missing.append(filename)

		filename = os.path.abspath(options.indir) + "/" + filenames[i] + "_r2.fastq.gz"
		if not os.path.exists(filename):
			filenames_missing.append(filename)

if filenames_missing:
	print("Error: the following file(s) are not in the bam directory.")
	print("\n".join(filenames_missing))
	exit(1)


print("PASS: All of those in the list file are intact.")
if not options.inputIsFastq:
	print("   Number of bam files: " + str(len(filenames)))
else:
	print("   Number of pairs of fastq files: " + str(len(filenames)))
print("   Number of samples: " + str(len(set(sampleids))))

exit(0)

# # Create symlinks
# if not os.path.exists(options.outdir):
#     os.makedirs(options.outdir)
# else:
# 	shutil.rmtree(options.outdir)
# 	os.makedirs(options.outdir)	
# 	# print("Error: Directory exists. Please delete the directory before proceeding.")
# 	# exit(1)

# for i in range(len(filenames)):
# 	os.symlink(os.path.abspath(options.indir) + "/" + filenames[i] + ".bam", options.outdir + "/" + sampleids[i] + ".bam")


# if len(offending_samplesids) != 0:
#     print(RED + "There are missing pair(s) in the Illumina sequences. Check your files and labelling before continuing. If this was intentional, use -f to write the file with duplicate names." + ENDC)
#     print(RED + "Offending sample ID(s): " + ", ".join(offendingSampleIDs) + ENDC)
#     print(RED + "Exiting..." + ENDC)
#     exit(0)





