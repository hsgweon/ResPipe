#!/usr/bin/env nextflow
/*
 ____             ____   _              
|  _ \  ___  ___ |  _ \ (_) _ __    ___ 
| |_) |/ _ \/ __|| |_) || || '_ \  / _ \
|  _ <|  __/\__ \|  __/ | || |_) ||  __/
|_| \_\\___||___/|_|    |_|| .__/  \___|
                            |_|          

ResPipe
Copyright (C) 2018 Hyun Soon Gweon
        
This script is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This script is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this script. If not, see <http://www.gnu.org/licenses/>.

For any bugs or problems found, please contact us at:
H. Soon Gweon <h.s.gweon@reading.ac.uk>

*/

// Version and help
params.version = false
params.help = false

// Configurable variables
params.in = false
params.out = "ResPipe_amr"

params.C_only = false // CARD only
params.I_only = false // ISfinder only
params.N_only = false // NCBI Enterobacteriaceae Plasmid only
params.M_only = false // Misc reference only
params.thermus = false


// Print version if asked
if (params.version) {
	System.out.println("ResPipe - Version: $VERSION ($TIMESTAMP)")
	exit 1
}

// Print help if asked
if (params.help) {
	System.out.println("Usage:")
    System.out.println("   nextflow run ResPipe_AMR.nf --in FASTQ_DIR")
	System.out.println("                [options] [--thermus HB27|HB8]")
    System.out.println("                [options] [--extract16S]")
    System.out.println("                [options] [--countSCG]")
	System.out.println("                [options] [-profile PROFILE]")
	exit 1
}


/////////////////////
// Validate inputs //
/////////////////////

if (!params["in"]) {
  println ""
  println "Error: Please use \"--in\" to specify the directory with your Trim-galore'd sequences. See README for more details."
  println ""
  exit 1
} 


/////////////////////////////
// Log dependency versions //
/////////////////////////////

process retrieve_dependencies_versions {

    publishDir "${params.out}", mode: 'copy'

    output:
    file "*.txt"

    script:
    """
    echo $VERSION > v_ResPipe.txt
    samtools --version > v_samtools.txt
    bedtools --version > v_bedtools.txt
    bbversion.sh > v_bbmap.txt
    """

}


//////////////////////
// Initial channels //
//////////////////////

Channel
    .fromPath("${params.in}/*.fq")
    .map { file -> [file.baseName.split(/_r\d(_unpaired_|_val_)/)[0], file] }
    .groupTuple()
    .set { input_fastqs }
    // .subscribe { println "vvalue: $it" }


/////////////////////////////
// Map reads to genes etc. //
/////////////////////////////

ref_CARD_metadata = "${baseDir}/data/ResPipe_CARD-${CARD_VERSION}.meta.tsv"

ref_CARD = "${baseDir}/data/ResPipe_CARD-${CARD_VERSION}.fasta"
ref_ISfinder = "${baseDir}/data/ResPipe_ISfinder.fasta"
ref_NCBIEnterobacterPlasmids = "${baseDir}/data/ResPipe_NCBIEnterobacterPlasmids.fasta"
ref_CustomFasta = "${baseDir}/data/CustomFasta"

refs = [ "CARD"                      : ref_CARD, 
         "ISfinder"                  : ref_ISfinder, 
         "NCBIEnterobacterPlasmids"  : ref_NCBIEnterobacterPlasmids ] // Don't put "_" in the name of the ref database!

if (params["C_only"] == true) {
    refs = ["CARD" : ref_CARD]
} else if (params["I_only"] == true) {
    refs = ["ISfinder" : ref_ISfinder]
} else if (params["N_only"] == true) {
    refs = ["NCBIEnterobacterPlasmids" : ref_NCBIEnterobacterPlasmids]
} else if (params["M_only"] == true) {
    refs = [ "CustomFasta" : ref_CustomFasta ]
}


ref_ThermusThermophilus = false
mask_ThermusThermophilus = false

if (params["thermus"] == "HB27"){
    ref_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB27__NC_005835.fasta"
    mask_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB27__NC_005835_mask.bed"
    refs << ["ThermusThermophilus" : ref_ThermusThermophilus]
} else if (params["thermus"] == "HB8"){
    ref_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB8__NC_006461.fasta"
    mask_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB8__NC_006461_mask.bed"
    refs << ["ThermusThermophilus" : ref_ThermusThermophilus]
} else if (params["thermus"] == false) {
    ref_ThermusThermophilus = false
    mask_ThermusThermophilus = false
} else {
    exit 1, "Unrecognised Thermus strain. Please select either \"--thermus HB27\" or \"--thermus HB8\""
}

process map_reads {

    tag "sample: ${run_id}, ref: ${ref}, CPUs: ${task.cpus}, files: ${in_files}"

    label "lowMemory"
    label "parallelisable"

    publishDir "${params.out}/1_sams_original_mapping", mode: 'copy'

    input:
    set val(run_id), file(files) from input_fastqs
    each ref from refs.keySet()

    output:
    file "${ref}_${run_id}.sam" into p_appraise_sams
    // file "${ref}_${run_id}.sam" into p_appraise_sams

    script:
    in_files = "${files}"
    out_file = "${ref}_${run_id}.sam"
    ref_path = refs[ref]

    """
    cat $in_files | bbmapskimmer.sh -Xmx${task.memory.toGiga()}g in=stdin.fq outm=$out_file ref=$ref_path ambig=all saa=f sam=1.3 semiperfectmode=t int=f t=${task.cpus} nodisk
    """

}


///////////////////
// The Appraisal //
/////////////////// Only at this stage, BEDTOOLS INTERSECT called for TT

process appraise_sams {

    maxForks 3

    tag "ref_sample: ${ref_sample}"

    label "moderateMemory"

    //publishDir "${params.out}/3_count_for_each_sample", mode: 'copy'

    input:
    file ref_sample from p_appraise_sams

    output:
    file("*.tsv") into c_create_tables

    script:
    filename = "${ref_sample}".tokenize(".")[0]
    ref = "${ref_sample}".tokenize("_")[0]
    ref_path = refs[ref]

    if ( ref == "CARD" )
        """
        ResPipe_SAM2REF.py             -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path}
        ResPipe_SAM2REF_hierachical.py -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path} --meta_data ${ref_CARD_metadata}
        rm -f ${filename}.sam
        """
    else if ( ref == "ISfinder" || ref == "NCBIEnterobacterPlasmids")
    	"""
        ResPipe_SAM2REF.py -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path}
        rm -f ${filename}.sam
        """
    else if ( ref == "ThermusThermophilus" )
    	"""
        samtools view -b -S -o ${filename}_unmasked.bam ${filename}.sam
        bedtools intersect -a ${filename}_unmasked.bam -b $mask_ThermusThermophilus -v > ${filename}_masked.bam
        samtools view -h -o ${filename}_masked.sam ${filename}_masked.bam
        ResPipe_SAM2REF.py -i ${filename}_masked.sam -o ${filename} --ref_fasta ${ref_path}
        rm -f ${filename}_unmasked.bam ${filename}_masked.bam ${filename}_masked.sam
        """
    else if ( ref == "CustomFasta" )
    	"""
        ResPipe_SAM2REF.py -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path}
        rm -f ${filename}.sam
        """
    else
	error "Invalid alignment ref: ${mode}"

}


/////////////////////////////////////////////////////
// Pull all results and combine into one big table //
/////////////////////////////////////////////////////

process create_tables {

    publishDir "${params.out}", mode: 'copy'

    input:
    file(files) from c_create_tables.collect()

    output:
    file("*.tsv")

    script:
    """
    ResPipe_combineTSVs.py --glob "CARD_*_read_count.tsv" -o "CARD_read_count.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_read_count_specific.tsv" -o "CARD_read_count_specific.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_lateral_coverage.tsv" -o "CARD_lateral_coverage.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_lateral_coverage_specific.tsv" -o "CARD_lateral_coverage_specific.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_read_lengths.tsv" -o "CARD_read_lengths.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_read_lengths_specific.tsv" -o "CARD_read_lengths_specific.tsv"

    ResPipe_combineTSVs.py --glob "CARD_*_read_count_hierachical.tsv" -o "CARD_read_count_hierachical.tsv"

    ResPipe_combineTSVs.py --glob "ISfinder_*_read_count.tsv" -o "ISfinder_read_count.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_read_count_specific.tsv" -o "ISfinder_read_count_specific.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_lateral_coverage.tsv" -o "ISfinder_lateral_coverage.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_lateral_coverage_specific.tsv" -o "ISfinder_lateral_coverage_specific.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_read_lengths.tsv" -o "ISfinder_read_lengths.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_read_lengths_specific.tsv" -o "ISfinder_read_lengths_specific.tsv"

    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_count.tsv" -o "NCBIEnterobacterPlasmids_read_count.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_count_specific.tsv" -o "NCBIEnterobacterPlasmids_read_count_specific.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_lateral_coverage.tsv" -o "NCBIEnterobacterPlasmids_lateral_coverage.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_lateral_coverage_specific.tsv" -o "NCBIEnterobacterPlasmids_lateral_coverage_specific.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_lengths.tsv" -o "NCBIEnterobacterPlasmids_read_lengths.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_lengths_specific.tsv" -o "NCBIEnterobacterPlasmids_read_lengths_specific.tsv"

    ResPipe_combineTSVs.py --glob "ThermusThermophilus_*_read_count.tsv" -o "ThermusThermophilus_read_count.tsv"
    ResPipe_combineTSVs.py --glob "ThermusThermophilus_*_lateral_coverage.tsv" -o "ThermusThermophilus_lateral_coverage.tsv"

    ResPipe_combineTSVs.py --glob "CustomFasta_*_read_count.tsv" -o "CustomFasta_read_count.tsv"
    ResPipe_combineTSVs.py --glob "CustomFasta_*_lateral_coverage.tsv" -o "CustomFasta_lateral_coverage.tsv"
    """

}

