#!/usr/bin/env/python
import argparse
import os

# process cmd line arguments
parser = argparse.ArgumentParser(description='write a samplefile for an input directory')
parser.add_argument('-d',dest='sampleDir',help='Directory containing sample bams',type=str,default=None)
parser.add_argument('-o',dest='outputFile',help='Output file',type=str,default="output")
args=parser.parse_args()

if not (args.sampleDir):
    parser.print_help()
    exit()
else:
    currentSamples=dict()
    if (args.sampleDir):
        # Check whether absolute or relative path
        if (args.sampleDir[0] == '/'):
            sampleDirPath = args.sampleDir
        else:
            sampleDirPath = os.path.join('.',args.sampleDir)
        
        if (os.path.isdir(sampleDirPath)):
            with open(os.path.join('.', args.outputFile),"w") as output:
                for file in os.listdir(sampleDirPath):
                    samplePath = os.path.join(sampleDirPath,file)
                    if os.path.isfile(samplePath):
                        ext = os.path.splitext(samplePath)[1]
                        # Check it is a valid fastq or gz (could be gzipped fastq)
                        if not (ext == '.fastq' or ext == '.fq' or ext == '.gz'):
                            print 'File {} not a valid fastq, skipping'.format(samplePath)
                            break
                        else:
                            # Check if valid gzipped fastq
                            if (ext == '.gz'):
                                try:
                                    firstext = os.path.basename(os.path.splitext(os.path.splitext(samplePath)[0])[1])
                                    if not (firstext == '.fastq' or firstext == '.fq'):
                                        print 'File {} not a valid fastq, skipping'.format(samplePath)
                                        break
                                    else:
                                        # record the filename (without extensions)
                                        sampleFile = os.path.basename(os.path.splitext(os.path.splitext(samplePath)[0])[0])
                                except Exception as e:
                                    print 'File {} not a valid fastq, skipping'.format(samplePath)
                                    break
                            else:
                                # record the filename (without extensions)
                                sampleFile = os.path.splitext(samplePath)[0]

                            try:
                                # record the sampleName (without read identifiers)
                                sampleSplit = str(sampleFile).split("_")
                                sampleName = ''
                                for splitLoc in range(len(sampleSplit)-1, 0, -1):
                                    splitStr = sampleSplit[splitLoc]
                                    if splitStr.lower() == '1' or splitStr.lower() == '2' or splitStr.lower() == 'r1' or splitStr.lower() == 'r2':
                                        sampleName = '_'.join(sampleSplit[0:splitLoc])
                                        break

                                 # output in format "XXXXX_r1/2<tab>XXXXX"
                                if sampleName == '':
                                    print "Couldn't parse read type of file {}.\nPlease check it is it format _*{{1/forward,2/reverse}}".format(str(file))
                                else:
                                    output.write(sampleName + "_checked\t" + sampleName + "\n")
                            except Exception as e:
                               print "Couldn't parse read type of file {}.\nPlease check it is it format _*{{1/forward,2/reverse}}".format(str(file))
    else:
        print 'No suitable input found. Exiting'
        exit()
