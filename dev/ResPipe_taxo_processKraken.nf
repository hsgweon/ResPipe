#!/usr/bin/env nextflow
/*
 ____             ____   _              
|  _ \  ___  ___ |  _ \ (_) _ __    ___ 
| |_) |/ _ \/ __|| |_) || || '_ \  / _ \
|  _ <|  __/\__ \|  __/ | || |_) ||  __/
|_| \_\\___||___/|_|    |_|| .__/  \___|
                            |_|          

ResPipe
Copyright (C) 2018 Hyun Soon Gweon
        
This script is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This script is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this script. If not, see <http://www.gnu.org/licenses/>.

For any bugs or problems found, please contact us at:
H. Soon Gweon <h.s.gweon@reading.ac.uk>

*/

// Version and help
params.version = false
params.help = false

// Configurable variables
params.in = false
params.out = "ResPipe_taxo_krakenReport"
params.listfile	= false

//	Print version if asked
if (params.version) {
	System.out.println("ResPipe - Version: $VERSION ($TIMESTAMP)")
	exit 1
}

// Print help if asked
if (params.help) {
	System.out.println("")
	System.out.println("")
	System.out.println("Usage: ")
	System.out.println("   nextflow run ResPipe_taxo.nf --in FASTQ_DIR --listfile LISTFILE")
	System.out.println("                [options] [--out OUTPUT_DIR] [-profile PROFILE]")
	System.out.println("")
	System.out.println("")
    exit 1
}

// Validate inputs
if (!params["in"]) {
  println ""
  println "Error: Please specify the directory with Kraken files with \"--in\". See README for more details."
  println ""
  exit 1
} 

if (params["listfile"]) {
    if( !file(params.listfile).exists() ) exit 1, "File not found: ${params.listfile}"
} else {
	exit 1, "Please specify the location of listfile files with \"--listfile\". See README for more details."
} 


/////////////////////////////
// Log dependency versions //
/////////////////////////////

process retrieve_dependencies_versions {

    publishDir "${params.out}/_dependencies", mode: 'copy'

    output:
    file "*.txt" into software_versions_yaml

    script:
    """
    echo $VERSION > ResPipe.txt
    """
}


//////////////////////
// Initial channels //
//////////////////////

Channel // Kraken files
	.fromPath("${params.in}/*_ps.kraken")
	.ifEmpty { error "Cannot find any kraken files in ${params.in}" }
	.map { file -> [file.baseName.split("_ps")[0], file] }
	.set { c_krakenCollected }

Channel // list file
	.fromPath(params.listfile)
	.splitCsv(sep: "\t")
	.map { row -> [ row[0], row[1] ] }
	.set { c_sampleGroup_kraken }

c_sampleGroup_kraken.cross( c_krakenCollected )
	.map { it -> tuple( it[0][1], it[1][1] ) }
	.groupTuple(by:0)
	.set { p_report_kraken }

process report_kraken {

	publishDir "${params.out}", 
		mode: 'copy', 
		saveAs: { filename ->
			if( filename.indexOf(".kraken.report") > 0 )
			    return "kraken/$filename"
			else if( filename.indexOf(".bracken.report") > 0)
				return "bracken/$filename"
			else if( filename.indexOf(".bracken.output") > 0)
				return "bracken/$filename"
			else
				return null
		}

	tag { "sample: $sample" }

	input:
	set sample, file(files) from p_report_kraken

	output:
	set sample, file("${sample}.kraken.report"), file("${sample}.bracken.report"), file("${sample}.bracken.output")

	script:
	"""
	cat $files > ${sample}.kraken
	kraken-report --db $KRAKEN_DB ${sample}.kraken > ${sample}.kraken.report

	est_abundance.py -i ${sample}.kraken.report -k $BRAKEN_DISTRIB -t 1 -o ${sample}.bracken.output	
	mv ${sample}.kraken_bracken.report ${sample}.bracken.report
	"""

}

	// cat $files > ${sample}.kraken
	// touch ${sample}.kraken.report
	// touch ${sample}.bracken.output
	// touch ${sample}.bracken.report





workflow.onComplete {
    println "Completed at: $workflow.complete"
    println "Execution status: ${ workflow.success ? 'OK' : 'failed' }"
}

