#!/usr/bin/env python

import argparse
import collections

parser = argparse.ArgumentParser("")
parser.add_argument("-i",
                    action = "store", 
                    dest = "infile", 
                    metavar = "infile",
                    help = "[REQUIRED]", 
                    required = True)
parser.add_argument("-o",
                    action = "store",
                    dest = "outfile",
                    metavar = "outfile",
                    help = "[REQUIRED]",
                    required = True)
options = parser.parse_args()

filenames = (options.infile).split(",")

# Reference (the first file)
reference_dict = collections.OrderedDict()

with open(filenames[0], "r") as reference_infile:
	for line in reference_infile:
		reference_dict[line.split("\t")[4]] = line.strip() # column [4] is taxid, i.e. the unique number for species

# Aggregate the rest
for i in range(1,len(filenames)):

	infile = open(filenames[1], "r")

	for line in infile:

		line = line.strip()
		taxid = line.split("\t")[4]

		reference_line = reference_dict[taxid]

		# Aggregate two lines
		r1 = reference_line.split("\t")[1]
		r2 = reference_line.split("\t")[2]
		r3 = reference_line.split("\t")[3]
		r4 = reference_line.split("\t")[4]
		r5 = reference_line.split("\t")[5]

		c1 = line.split("\t")[1]
		c2 = line.split("\t")[2]
		c3 = line.split("\t")[3]
		c4 = line.split("\t")[4]
		c5 = line.split("\t")[5]

		# Sanity check
		if r3 != c3 or r4 != c4 or r5 != c5:
			print("error!!!")
			exit(1)

		m1 = int(r1) + int(c1)
		m2 = int(r2) + int(c2)

		reference_dict[taxid] = "\t".join(["", str(m1), str(m2), r3, r4, r5]) # reference dictionary is refreshed with aggregated number

	infile.close()

# Total reads
total_number_of_reads = int(reference_dict["0"].split("\t")[1]) + int(reference_dict["1"].split("\t")[1])

# Output
outfile = open(options.outfile, "w")

for k, v in reference_dict.items():

        if total_number_of_reads != 0:
                percentage = float(v.split("\t")[1]) * 100 / total_number_of_reads
        else:
                percentage = 0

        percentage = "%.2f" % round(percentage, 2)
        outfile.write(percentage + "\t" + "\t".join(v.split("\t")[1:]) + "\n")

outfile.close()
