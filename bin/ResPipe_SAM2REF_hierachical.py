#!/usr/bin/env python
#
#  ____             ____   _
# |  _ \  ___  ___ |  _ \ (_) _ __    ___
# | |_) |/ _ \/ __|| |_) || || '_ \  / _ \
# |  _ <|  __/\__ \|  __/ | || |_) ||  __/
# |_| \_\\___||___/|_|    |_|| .__/  \___|
#                             |_|
#
# ResPipe
# Copyright (C) 2017 Hyun Soon Gweon
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.
#
# For any bugs or problems found, please contact us at:
# H. Soon Gweon <h.s.gweon@reading.ac.uk>

import argparse

parser = argparse.ArgumentParser("")
parser.add_argument(
    "-i",
    action="store",
    dest="infile",
    metavar="infile",
    help="[REQUIRED] SAM file",
    required=True,
)
parser.add_argument(
    "-o",
    action="store",
    dest="outfile",
    metavar="outfile",
    help="[REQUIRED] Output prefix",
    required=True,
)
parser.add_argument(
    "--ref_fasta",
    action="store",
    dest="ref_fasta",
    metavar="ref_fasta",
    help="[REQUIRED] Reference file (FASTA). This is needed for calculating coverage",
    required=True,
)
parser.add_argument(
    "--meta_data",
    action="store",
    dest="meta_data",
    metavar="meta_data",
    help="[REQUIRED] Metadata",
    required=True,
)
options = parser.parse_args()

import re, itertools, operator

#############################
# Deal with reference FASTA #
#############################

reference_id = []
reference_length = {}

read_count = {}
read_count_specific = {}

lateral_coverage = {}
lateral_coverage_specific = {}

AMR_count_hierachical_NM0 = {}  # Another way of counting AMR
AMR_count_hierachical_ALL = {}


# This is needed to store read lengths values for the mapped for each RP
read_lengths = {}
read_lengths_specific = {}


def fasta_iter(fasta_name):

    fasta = (
        x[1] for x in itertools.groupby(open(fasta_name), lambda line: line[0] == ">")
    )

    for group in fasta:
        header = "".join(i.strip() for i in group)
        if header.count(">") > 1:
            print("Empty entry in the FASTA file: " + header)
            exit(1)
        sequence = "".join(i.strip() for i in next(fasta))
        yield (header[1:], len(sequence))

f_iter = fasta_iter(options.ref_fasta)

for entry in f_iter:
    header, length = entry
    reference_id.append(header)
    reference_length[header] = length


############
# Metadata #
############

CARD_metadata_ARO_name = {}
CARD_metadata_AMR_Gene_Family = {}

with open(options.meta_data) as infile:

    for line in infile:

        ResPipeUI = line.split("\t")[0]
        ARO_name = line.split("\t")[2]
        AMR_Gene_Family = line.split("\t")[3]

        CARD_metadata_ARO_name[ResPipeUI] = ARO_name
        CARD_metadata_AMR_Gene_Family[ResPipeUI] = AMR_Gene_Family


#########################################
# Fill abundance and coverage with refs #
#########################################

for i in reference_id:

    read_count[i] = 0
    read_count_specific[i] = 0

    lateral_coverage[i] = [0] * reference_length[i]
    lateral_coverage_specific[i] = [0] * reference_length[i]

    read_lengths[i] = list()
    read_lengths_specific[i] = list()


#################################################
# Create a dic of Key:QNAME_FRAG Value:all hits #
#################################################

DNAFragment = (
    {}
)  # For each fragment (as in a long stretch of DNA sequenced from both ends.)

with open(options.infile) as infile:

    for line in infile:

        if not line.startswith("@"):

            l = line.rstrip().split("\t")

            QNAME = l[0]  # Read name
            QNAME_FRAG = QNAME.split("/")[0]  # Fragment name
            QNAME_FORWARD_OR_REVERSE = int(QNAME.split("/")[1])
            RNAME = l[2]  # Reference name
            START_POS = int(l[3])  # Start position
            MAPQ = l[4]

            CIGAR = re.findall(r"(\d+)([A-Z]{1})", l[5])
            CIGAR_map = {}

            for m in CIGAR:
                CIGAR_map[m[1]] = int(m[0])

            LENGTH = CIGAR_map["M"]

            NM = 0
            for tag in l:
                if tag.startswith("NM:i:"):
                    NM = int(tag.split(":i:")[1])
                    break
                if NM == "":
                    print("NM not found")
                    exit(1)

            hit = [QNAME, RNAME, START_POS, LENGTH, CIGAR_map, NM]

            # For some reason Trim_galore! still outputs short reads. So here is an additional filter. We will go with 100bps.
            if LENGTH >= 100:
                if QNAME_FRAG in DNAFragment:
                    DNAFragment[QNAME_FRAG].append(hit)
                else:
                    DNAFragment[QNAME_FRAG] = [hit]


########################
# Now process the dict #
########################

# Non-specific
# A. Read_F maps to Ref_1 & Read_R maps to Ref_2 -> Ref_1++ & Ref_2 ++ ; coverage accounted for both
# B. Read_F maps to Ref_1 & Read_R also maps to Ref_1 -> Ref_1++ (once) ; coverage accounted for both
#
# Specific counting:
# A. Read_F maps to Ref_1 & Read_R maps to Ref_2 -> nothing is counted. At the end of the day, the fragment maps to more than one ref.
#    This is actually very interesting special case, as this may indicate that such a gene is present in sample.
# B. Read_F maps to Ref_1 & Read_R also maps to Ref_1 -> Ref_1++ (once) ; coverage accounted for both


def splitAlignmentIntoRefs(
    ll
):  
    # Return a list of lists. Each list will have members representing different refs.
    returned_list = []
    
    # Sort by [1] - reference name
    ll.sort(key=operator.itemgetter(1))
    for key, group in itertools.groupby(
        ll, lambda t: t[1]
    ):  # Group by reference by name
        # Sort by Frag ID first, then by the length of the alignment.
        # I.e. if length is the same, choose READ 1
        group = sorted(
            group, key=operator.itemgetter(0)
        )  # Sort by QNAME i.e. Forward read first, then Reverse read
        group = sorted(
            group, key=operator.itemgetter(3), reverse=True
        )  # Then sort by length.
        # print(key)
        # print(group)
        returned_list.append(group)
    # print("_______________________")
    return returned_list


def assign_LCA(
    ll
):  
    # Return a list of lists. Each list will have members representing different refs.
    segments = []

    F_NM0_mapped_ResPipeUI = []
    F_NM0_mapped_ARO_names = []
    F_NM0_mapped_ARO_Gene_Family = []
    R_NM0_mapped_ResPipeUI = []
    R_NM0_mapped_ARO_names = []
    R_NM0_mapped_ARO_Gene_Family = []

    F_NMX_mapped_ResPipeUI = []
    F_NMX_mapped_ARO_names = []
    F_NMX_mapped_ARO_Gene_Family = []
    R_NMX_mapped_ResPipeUI = []
    R_NMX_mapped_ARO_names = []
    R_NMX_mapped_ARO_Gene_Family = []

    # Check if this fragment has forward only or reverse only or both.
    for i in ll:

        segment = int(i[0][-1])  # 1: Forward, 2: Reverse
        segments.append(segment)

        NM = i[5]
        mapped_ref = i[1]

        if NM == 0:
            if segment == 1:
                F_NM0_mapped_ResPipeUI.append(mapped_ref)
                F_NM0_mapped_ARO_names.append(CARD_metadata_ARO_name[mapped_ref])
                F_NM0_mapped_ARO_Gene_Family.append(
                    CARD_metadata_AMR_Gene_Family[mapped_ref]
                )
            else:
                R_NM0_mapped_ResPipeUI.append(mapped_ref)
                R_NM0_mapped_ARO_names.append(CARD_metadata_ARO_name[mapped_ref])
                R_NM0_mapped_ARO_Gene_Family.append(
                    CARD_metadata_AMR_Gene_Family[mapped_ref]
                )
        else:
            if segment == 1:
                F_NMX_mapped_ResPipeUI.append(mapped_ref)
                F_NMX_mapped_ARO_names.append(CARD_metadata_ARO_name[mapped_ref])
                F_NMX_mapped_ARO_Gene_Family.append(
                    CARD_metadata_AMR_Gene_Family[mapped_ref]
                )
            else:
                R_NMX_mapped_ResPipeUI.append(mapped_ref)
                R_NMX_mapped_ARO_names.append(CARD_metadata_ARO_name[mapped_ref])
                R_NMX_mapped_ARO_Gene_Family.append(
                    CARD_metadata_AMR_Gene_Family[mapped_ref]
                )

        # Combined
        NM0_mapped_ResPipeUI = F_NM0_mapped_ResPipeUI + R_NM0_mapped_ResPipeUI
        NM0_mapped_ARO_names = F_NM0_mapped_ARO_names + R_NM0_mapped_ARO_names
        NM0_mapped_ARO_Gene_Family = (
            F_NM0_mapped_ARO_Gene_Family + R_NM0_mapped_ARO_Gene_Family
        )

        NMX_mapped_ResPipeUI = F_NMX_mapped_ResPipeUI + R_NMX_mapped_ResPipeUI
        NMX_mapped_ARO_names = F_NMX_mapped_ARO_names + R_NMX_mapped_ARO_names
        NMX_mapped_ARO_Gene_Family = (
            F_NMX_mapped_ARO_Gene_Family + R_NMX_mapped_ARO_Gene_Family
        )

    if len(set(segments)) == 1:  # Only F or R?

        NM = ""
        classification = ""

        # Either it's NM0 or NMX. i.e. it has exact hit with a reference or not.
        if (
            len(set(NM0_mapped_ResPipeUI)) > 0
            and len(set(NM0_mapped_ARO_names)) > 0
            and len(set(NM0_mapped_ARO_Gene_Family)) > 0
        ):

            if len(set(NM0_mapped_ResPipeUI)) == 1:
                ResPipeUI = NM0_mapped_ResPipeUI[0]
            else:
                ResPipeUI = "other"

            if len(set(NM0_mapped_ARO_names)) == 1:
                ARO_names = NM0_mapped_ARO_names[0]
            else:
                ARO_names = "other"

            if len(set(NM0_mapped_ARO_Gene_Family)) == 1:
                ARO_Gene_Family = NM0_mapped_ARO_Gene_Family[0]
            else:
                ARO_Gene_Family = "other"

            NM = "NM0"

        else:

            if len(set(NMX_mapped_ResPipeUI)) == 1:
                ResPipeUI = NMX_mapped_ResPipeUI[0]
            else:
                ResPipeUI = "other"

            if len(set(NMX_mapped_ARO_names)) == 1:
                ARO_names = NMX_mapped_ARO_names[0]
            else:
                ARO_names = "other"

            if len(set(NMX_mapped_ARO_Gene_Family)) == 1:
                ARO_Gene_Family = NMX_mapped_ARO_Gene_Family[0]
            else:
                ARO_Gene_Family = "other"

            NM = "NMX"

    # Both F and R map to a refence
    else:

        # Both F and R have NM0. Only this is classified as NM0 match (strict mapping)
        if len(F_NM0_mapped_ResPipeUI) > 0 and len(R_NM0_mapped_ResPipeUI) > 0:

            if len(set(NM0_mapped_ResPipeUI)) == 1:
                ResPipeUI = NM0_mapped_ResPipeUI[0]
            else:
                ResPipeUI = "other"

            if len(set(NM0_mapped_ARO_names)) == 1:
                ARO_names = NM0_mapped_ARO_names[0]
            else:
                ARO_names = "other"

            if len(set(NM0_mapped_ARO_Gene_Family)) == 1:
                ARO_Gene_Family = NM0_mapped_ARO_Gene_Family[0]
            else:
                ARO_Gene_Family = "other"

            NM = "NM0"

        else:

            NMA_mapped_ResPipeUI = NM0_mapped_ResPipeUI + NMX_mapped_ResPipeUI
            NMA_mapped_ARO_names = NM0_mapped_ARO_names + NMX_mapped_ARO_names
            NMA_mapped_ARO_Gene_Family = (
                NM0_mapped_ARO_Gene_Family + NMX_mapped_ARO_Gene_Family
            )

            if len(set(NMA_mapped_ResPipeUI)) == 1:
                ResPipeUI = NMA_mapped_ResPipeUI[0]
            else:
                ResPipeUI = "other"

            if len(set(NMA_mapped_ARO_names)) == 1:
                ARO_names = NMA_mapped_ARO_names[0]
            else:
                ARO_names = "other"

            if len(set(NMA_mapped_ARO_Gene_Family)) == 1:
                ARO_Gene_Family = NMA_mapped_ARO_Gene_Family[0]
            else:
                ARO_Gene_Family = "other"

            NM = "NMX"

    classification = ARO_Gene_Family + "___" + ARO_names + "___" + ResPipeUI

    return [NM, classification]


###############################
# For each unique QNAME_FRAG: #
###############################

for fragment, alignments in DNAFragment.items():

    # LCA
    assignment = assign_LCA(alignments)
    if assignment[0] == "NM0":
        if assignment[1] in AMR_count_hierachical_NM0:
            AMR_count_hierachical_NM0[assignment[1]] += 1
        else:
            AMR_count_hierachical_NM0[assignment[1]] = 1

    # Liberal counting (allowing NM > 0)
    if assignment[1] in AMR_count_hierachical_ALL:
        AMR_count_hierachical_ALL[assignment[1]] += 1
    else:
        AMR_count_hierachical_ALL[assignment[1]] = 1


##########
# Output #
##########

outfile_LCA_count_NM0 = open(options.outfile + "_read_count_hierachical.tsv", "w")

# Add column label to each header
outfile_LCA_count_NM0.write("\t" + options.outfile + "\n")

for LCA, count in sorted(AMR_count_hierachical_NM0.items()):
    outfile_LCA_count_NM0.write(LCA + "\t" + str(count) + "\n")

# Close files
outfile_LCA_count_NM0.close()

exit(0)
