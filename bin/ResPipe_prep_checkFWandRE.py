#!/usr/bin/env python
#
#  ____             ____   _              
# |  _ \  ___  ___ |  _ \ (_) _ __    ___ 
# | |_) |/ _ \/ __|| |_) || || '_ \  / _ \
# |  _ <|  __/\__ \|  __/ | || |_) ||  __/
# |_| \_\\___||___/|_|    |_|| .__/  \___|
#                             |_|          
#
# ResPipe
# Copyright (C) 2017 Hyun Soon Gweon
#        
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.
#
# For any bugs or problems found, please contact us at:
# H. Soon Gweon <h.s.gweon@reading.ac.uk>

import argparse
import gzip, io

###################################################
# Copy file, adding forward/ reverse if necessary #
###################################################
def checkFastq(infile, outfile, read_type):
    with io.BufferedReader( gzip.open(infile, 'rb')) as infile:
        with io.BufferedWriter( gzip.open(outfile, 'wb')) as outfile:
            linesToSkip = 0
            for line in infile:
                # process file in block to avoid picking up @ from quality etc
                if linesToSkip > 0:
                    linesToSkip+=-1
                    outfile.write(line)
                    continue

                outputline = ""
                if line.startswith(b"@"):
                    l = line.rstrip()
                    ls = l.split(b"/")         # Fragment name and fw/rw
                    if len(ls) < 2:
                        outputline = l + b"/" + str.encode(read_type) + b"\n"
                    else:
                        outputline = line
                else:
                    outputline = line
                
                outfile.write(outputline)
                linesToSkip = 3

parser = argparse.ArgumentParser("")
parser.add_argument("-i",
                    action = "store", 
                    dest = "infile", 
                    metavar = "infile",
                    help = "[REQUIRED] FASTQ file",
                    required = True)
parser.add_argument("-o",
                    action = "store",
                    dest = "outfile",
                    metavar = "outfile",
                    help = "[REQUIRED] Output prefix",
                    required = True)
parser.add_argument("-r",
                    action = "store",
                    dest = "read_type",
                    metavar = "read_type",
                    help = "[REQUIRED] Forward or reverse read",
                    required = True)
options = parser.parse_args()

if __name__ == "__main__":
    checkFastq(options.infile, options.outfile, options.read_type)
    exit(0)
