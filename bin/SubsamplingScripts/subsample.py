#!/usr/bin/env python

########################################
# Argument Options
import argparse
parser = argparse.ArgumentParser("")
parser.add_argument("-i",
					action = "store",
					dest = "infile",
					metavar = "infile",
					help = "[REQUIRED]",
					required = True)
parser.add_argument("-o",
					action = "store",
					dest = "outfile",
					metavar = "outfile",
					help = "[REQUIRED]",
					required = True)
parser.add_argument("--column",
					action = "store",
					dest = "column",
					metavar = "column",
					help = "[REQUIRED]",
					required = True)
parser.add_argument("-d",
					action = "store",
					dest = "depth",
					metavar = "depth",
					help = "[REQUIRED]",
					required = True)
parser.add_argument("--header",
					action = "store",
					dest = "header",
					metavar = "header",
					help = "[REQUIRED]",
					required = True)
parser.add_argument("--skip_first_line",
					action = "store_true",
					dest = "skip_first_line",
					default = True)
options = parser.parse_args()
########################################

import random
import collections
import pandas as pd

# Create a bag
bag = []
index = 1
with open(options.infile, "r") as infile:
	next(infile)		
	for line in infile:
		bag.extend([index] * int(float(line.split()[int(options.column)])))
		index += 1
bag.extend([index] * (200000000 - len(bag)))


df = pd.DataFrame()
depth = int(options.depth)

for i in range(10):

	print("Start: " + str(depth) + "\t" + str(i))

	header = "CARD_S-EF_D" + str(f"{depth:09}") + "-" + str(i + 11)

	rand_balls = random.sample(bag, depth)
	frequency = collections.Counter(rand_balls)

	subsampled = []
	for j in range(1, index):
		subsampled.append(frequency[j])

	se = pd.Series(subsampled)

	df[header] = se.values
		# with open(options.outfile, "w") as outfile:
		# 	outfile.write()
		# 	for i in range(1, index):
		# 		outfile.write(str(frequency[i]) + "\n")

df.to_csv(options.outfile, sep="\t", index=False)














