#!/usr/bin/env python
#
#  ____             ____   _              
# |  _ \  ___  ___ |  _ \ (_) _ __    ___ 
# | |_) |/ _ \/ __|| |_) || || '_ \  / _ \
# |  _ <|  __/\__ \|  __/ | || |_) ||  __/
# |_| \_\\___||___/|_|    |_|| .__/  \___|
#                             |_|
#
# ResPipe
# Copyright (C) 2017 Hyun Soon Gweon
# 
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.
#
# For any bugs or problems found, please contact us at:
# H. Soon Gweon <h.s.gweon@reading.ac.uk>

import argparse, glob, pandas as pd
parser = argparse.ArgumentParser("")
parser.add_argument("--glob",
                    action = "store", 
                    dest = "glob", 
                    metavar = "glob",
                    help = "[REQUIRED]",
                    required = True)
parser.add_argument("-o",
                    action = "store",
                    dest = "outfile",
                    metavar = "outfile",
                    help = "[REQUIRED]",
                    required = True)
options = parser.parse_args()

files = glob.glob(options.glob)
files = sorted(files)
if len(files) != 0:
    df = pd.DataFrame()
    for file in files:
        # columnName = "_".join(file.split("_")[1:])
        df_next = pd.read_csv(file, sep = "\t", index_col = 0, float_precision = 'round_trip').astype(float)
        df = pd.concat([df, df_next], axis = 1)
#        df = pd.concat([df, df_next], axis = 1, sort = True) # Change to this if using pandas 0.23... otherwise you will see a warning message.
        df.fillna(0, inplace = True)
        df.to_csv(options.outfile, sep = "\t", header = True, index = True)
