#!/usr/bin/env python
#
#  ____             ____   _              
# |  _ \  ___  ___ |  _ \ (_) _ __    ___ 
# | |_) |/ _ \/ __|| |_) || || '_ \  / _ \
# |  _ <|  __/\__ \|  __/ | || |_) ||  __/
# |_| \_\\___||___/|_|    |_|| .__/  \___|
#                             |_|          
#
# ResPipe
# Copyright (C) 2017 Hyun Soon Gweon
#        
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.
#
# For any bugs or problems found, please contact us at:
# H. Soon Gweon <h.s.gweon@reading.ac.uk>

import argparse
parser = argparse.ArgumentParser("")
parser.add_argument("-i",
                    action = "store", 
                    dest = "infile", 
                    metavar = "infile",
                    help = "[REQUIRED] SAM file",
                    required = True)
parser.add_argument("-o",
                    action = "store",
                    dest = "outfile",
                    metavar = "outfile",
                    help = "[REQUIRED] Output prefix",
                    required = True)
parser.add_argument("--ref_fasta",
                    action = "store",
                    dest = "ref_fasta",
                    metavar = "ref_fasta",
                    help = "[REQUIRED] Reference file (FASTA). This is needed for calculating coverage",
                    required = True)
options = parser.parse_args()

import re, itertools, operator

#############################
# Deal with reference FASTA #
#############################

reference_id = []
reference_length = {}

read_count = {}
read_count_specific = {}

lateral_coverage = {}
lateral_coverage_specific = {}

# This is needed to store read lengths values for the mapped for each RP
read_lengths = {}
read_lengths_specific = {}

def fasta_iter(fasta_name):
    fasta = (x[1] for x in itertools.groupby(open(fasta_name), lambda line:line[0] == ">"))
    for group in fasta:
        header = "".join(i.strip() for i in group)
        if header.count(">") > 1:
            print("Empty entry in the FASTA file: " + header)
            exit(1)
        sequence = "".join(i.strip() for i in next(fasta))
        yield(header[1:], len(sequence))
f_iter = fasta_iter(options.ref_fasta)
for entry in f_iter:
    header, length = entry
    reference_id.append(header)
    reference_length[header] = length


#########################################
# Fill abundance and coverage with refs #
#########################################

for i in reference_id:
    read_count[i] = 0
    read_count_specific[i] = 0

    lateral_coverage[i] = [0] * reference_length[i]
    lateral_coverage_specific[i] = [0] * reference_length[i]

    read_lengths[i] = list()
    read_lengths_specific[i] = list()


#################################################
# Create a dic of Key:QNAME_FRAG Value:all hits #
#################################################

DNAFragment = {} # For each fragment (as in a long stretch of DNA sequenced from both ends.)

with open(options.infile) as infile:

    for line in infile:
    
        if not line.startswith("@"):

            l = line.rstrip().split("\t")

            QNAME = l[0]                        # Read name
            QNAME_FRAG = QNAME.split("/")[0]    # Fragment name
            QNAME_FORWARD_OR_REVERSE = int(QNAME.split("/")[1])
            RNAME = l[2]                        # Reference name
            START_POS = int(l[3])                     # Start position
            MAPQ = l[4]
            
            CIGAR = re.findall(r'(\d+)([A-Z]{1})',l[5])
            CIGAR_map = {}

            for m in CIGAR:            
                CIGAR_map[m[1]] = int(m[0])
            
            LENGTH = CIGAR_map["M"]

            NM = 0
            # NM
            for tag in l:
                if tag.startswith("NM:i:"):
                    NM = int(tag.split(":i:")[1])
                    break
                if NM == "":
                    print("NM not found")
                    exit(1)

            hit = [QNAME, RNAME, START_POS, LENGTH, CIGAR_map, NM]

            # For some reason Trim_galore! still outputs short reads. So here is an additional filter. We will go with 100bps.
            if LENGTH >= 100:

                if QNAME_FRAG in DNAFragment:
                    DNAFragment[QNAME_FRAG].append(hit)
                else:
                    DNAFragment[QNAME_FRAG] = [hit]


########################
# Now process the dict #
########################

# Non-specific
# A. Read_F maps to Ref_1 & Read_R maps to Ref_2 -> Ref_1++ & Ref_2 ++ ; coverage accounted for both
# B. Read_F maps to Ref_1 & Read_R also maps to Ref_1 -> Ref_1++ (once) ; coverage accounted for both
#
# Specific counting:
# A. Read_F maps to Ref_1 & Read_R maps to Ref_2 -> nothing is counted. At the end of the day, the fragment maps to more than one ref. 
#    This is actually very interesting special case, as this may indicate that such a gene is present in sample.
# B. Read_F maps to Ref_1 & Read_R also maps to Ref_1 -> Ref_1++ (once) ; coverage accounted for both


def splitAlignmentIntoRefs(ll): # Return a list of lists. Each list will have members representing different refs.
    returned_list = []
    # Sort by [1] - reference name
    ll.sort(key = operator.itemgetter(1))    
    for key, group in itertools.groupby(ll, lambda t:t[1]): # Group by reference by name
        # Sort by Frag ID first, then by the length of the alignment.
        # I.e. if length is the same, choose READ 1
        group = sorted(group, key=operator.itemgetter(0)) # Sort by QNAME i.e. Forward read first, then Reverse read
        group = sorted(group, key=operator.itemgetter(3), reverse=True) # Then sort by length.
        # print(key)
        # print(group)
        returned_list.append(group)
    # print("_______________________")
    return returned_list


###############################
# For each unique QNAME_FRAG: #
###############################

for fragment, alignments in DNAFragment.items():

    alignmentsGroupedIntoRefs = splitAlignmentIntoRefs(alignments)

    # Check if both F and/or R has NM0. If so, pass
    alignmentsGroupedIntoRefs_NM0 = []
    for aRef in alignmentsGroupedIntoRefs:
        alignmentsGroupedIntoRefs_NM0.append(aRef)

    # Non-specific
    for aRef in alignmentsGroupedIntoRefs_NM0:

        refName = aRef[0][1]

        #########
        # Count #
        #########
        read_count[refName] += 1

        ##################
        # Average Length #
        ##################
        averageLength = sum([a[3] for a in aRef]) / float(len([a[3] for a in aRef]))
        read_lengths[refName].append(averageLength)

        ############
        # Coverage #
        ############
        for a in aRef:
            start_pos = a[2]-1
            length = start_pos + a[3]
            for i in range(start_pos, length):
                lateral_coverage[refName][i] += 1

    # Specific
    if len(alignmentsGroupedIntoRefs_NM0) == 1:

        firstAndOnlyRef = alignmentsGroupedIntoRefs_NM0[0]

        refName = firstAndOnlyRef[0][1]

        ##################
        # Specific Count #
        ##################
        read_count_specific[refName] += 1

        ##################
        # Average Length #
        ##################
        averageLength = sum([a[3] for a in firstAndOnlyRef]) / float(len([a[3] for a in firstAndOnlyRef]))
        read_lengths_specific[refName].append(averageLength)

        #####################
        # Specific Coverage #
        #####################
        for a in firstAndOnlyRef:
            start_pos = a[2]-1
            length = start_pos + a[3]
            for i in range(start_pos, length):
                lateral_coverage_specific[refName][i] += 1


##########
# Output #
##########

# open files
outfile_read_count = open(options.outfile + "_read_count.tsv", "w")
outfile_lateral_coverage = open(options.outfile + "_lateral_coverage.tsv", "w")
outfile_read_count_specific = open(options.outfile + "_read_count_specific.tsv", "w")
outfile_lateral_coverage_specific = open(options.outfile + "_lateral_coverage_specific.tsv", "w")
outfile_read_lengths = open(options.outfile + "_read_lengths.tsv", "w")
outfile_read_lengths_specific = open(options.outfile + "_read_lengths_specific.tsv", "w")

# Add column label to each header
outfile_read_count.write("\t" + options.outfile + "\n")
outfile_read_count_specific.write("\t" + options.outfile + "\n")
outfile_lateral_coverage.write("\t" + options.outfile + "\n")
outfile_lateral_coverage_specific.write("\t" + options.outfile + "\n")
outfile_read_lengths.write("\t" + options.outfile + "\n")
outfile_read_lengths_specific.write("\t" + options.outfile + "\n")

for i in reference_id:

    # Read count
    outfile_read_count.write(i + "\t" + str(read_count[i]) + "\n")
    outfile_read_count_specific.write(i + "\t" + str(read_count_specific[i]) + "\n")

    # Count number of non-zero locations (covered), then divide that by the length of the gene
    lateral_coverage_sum = sum(1 for x in lateral_coverage[i] if x > 0)
    lateral_coverage_specific_sum = sum(1 for x in lateral_coverage_specific[i] if x > 0)

    lateral_coverage_proportion = round(lateral_coverage_sum / len(lateral_coverage[i]), 3)
    lateral_coverage_specific_proportion = round(lateral_coverage_specific_sum / len(lateral_coverage_specific[i]), 3)

    outfile_lateral_coverage.write(i + "\t" + str(lateral_coverage_proportion) + "\n")
    outfile_lateral_coverage_specific.write(i + "\t" + str(lateral_coverage_specific_proportion) + "\n")

    # if i == "RP_001853":
    #     print(str(i), read_count_specific[i])
    #     print(lateral_coverage[i])
    #     print(lateral_coverage_specific[i])


    ########################
    # Average read lengths #
    ########################

    if len(read_lengths[i]) == 0:
        average_read_length = 0
    else:
        average_read_length = round(sum(read_lengths[i]) / float(len(read_lengths[i])), 3)

    if len(read_lengths_specific[i]) == 0:
        average_read_length_specific = 0
    else:
        average_read_length_specific = round(sum(read_lengths_specific[i]) / float(len(read_lengths_specific[i])), 3)

    outfile_read_lengths.write(i + "\t" + str(average_read_length) + "\n")
    outfile_read_lengths_specific.write(i + "\t" + str(average_read_length_specific) + "\n")


# Close files
outfile_read_count.close()
outfile_read_count_specific.close()
outfile_lateral_coverage.close()
outfile_lateral_coverage_specific.close()
outfile_read_lengths.close()
outfile_read_lengths_specific.close()

exit(0)
