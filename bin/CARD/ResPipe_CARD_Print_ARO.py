#!/usr/bin/env python

########################################
# Argument Options
import argparse
parser = argparse.ArgumentParser("")
parser.add_argument("-i",
					action = "store",
					dest = "input",
					metavar = "input",
					help = "[REQUIRED]",
					required = True)
options = parser.parse_args()
########################################

from Bio import SeqIO

for record in SeqIO.parse(options.input, "fasta"):

    header = record.description

    ARO = header.split("|")[4]
    gbi = header.split("|")[1]
    ARO_name = header.split("|")[5].split("[")[0].strip()
    species = header.split("|")[5].split("[")[1].split("]")[0].strip()

    # Check header integrity
    if not ARO.startswith("ARO:") or not header.startswith("gb"):
    	print(header)
    	exit(1)

    print(ARO.split(":")[1])
