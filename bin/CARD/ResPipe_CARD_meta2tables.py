#!/usr/bin/env python

import sys
import random
import collections
import pandas as pd
import numpy

####################
# Argument Options #
####################

import argparse
parser = argparse.ArgumentParser(description = "")
parser.add_argument("-i",
                    action = "store",
                    dest = "infile",
                    metavar = "infile",
                    help = "[REQUIRED] dataset",
                    required = True)

options = parser.parse_args()

infile = open(options.infile, "r")

ARO_accession_processed = []

# Dictionary to store values for each ARO_accession
Antibiotics = {}
DrugClasses = {}

# Store all antibiotics, drug_classes
all_Antibiotics = []
all_DrugClasses = []

for line in infile:

    if line.startswith("#"):
        continue

    # Check for duplicates
    ARO_accession = line.split("\t")[1]
    if ARO_accession in ARO_accession_processed:
        continue
    else:
        ARO_accession_processed.append(ARO_accession)

    # Dictionary of dictionary to store values for each antibiotic and drug class
    Antibiotics[ARO_accession] = {}
    DrugClasses[ARO_accession] = {}

    # Fields
    Antibiotics_per_ARO = line.split("\t")[6].split(";")
    DrugClasses_per_ARO = line.split("\t")[4].split(";")


    # Antibiotics
    for element in Antibiotics_per_ARO:

        value = 1

        if element.split("__")[0] == "confers_resistance_to_drug":
            value = 2
            Antibiotic = element.split("__")[1]
        else:
            Antibiotic = element.split("__")[0]

        Antibiotics[ARO_accession][Antibiotic] = value

        # Store antibiotics
        if Antibiotic not in all_Antibiotics:
            all_Antibiotics.append(Antibiotic)


    # Drug classes
    for element in DrugClasses_per_ARO:

        value = 1

        if element.split("__")[0] == "confers_resistance_to":
            value = 2
            DrugClass = element.split("__")[1]
        else:
            DrugClass = element.split("__")[0]

        DrugClasses[ARO_accession][DrugClass] = value

        if DrugClass not in all_DrugClasses:
            all_DrugClasses.append(DrugClass)


# Sort
all_Antibiotics = sorted(all_Antibiotics, key=str.lower)
all_DrugClasses = sorted(all_DrugClasses, key=str.lower)


# Output
l = [] # For printing


outfile_Antibiotics = open("ResPipe_CARD-X.X.X.meta.Antibiotics.csv", "w")
outfile_Antibiotics.write("," + ",".join(all_Antibiotics) + "\n") # Headers
for ARO in ARO_accession_processed:
    del l[:]
    l.append(ARO) # First column
    for a in all_Antibiotics:
        value = 0
        if a not in Antibiotics[ARO]:
            value = 0
        else: 
            value = Antibiotics[ARO][a]
        l.append(str(value))
    outfile_Antibiotics.write(",".join(l) + "\n")


outfile_DrugClasses = open("ResPipe_CARD-X.X.X.meta.DrugClasses.csv", "w")
outfile_DrugClasses.write("," + ",".join(all_DrugClasses) + "\n") # Headers
for ARO in ARO_accession_processed:
    del l[:]
    l.append(ARO) # First column
    for a in all_DrugClasses:
        value = 0
        if a not in DrugClasses[ARO]:
            value = 0
        else: 
            value = DrugClasses[ARO][a]
        l.append(str(value))
    outfile_DrugClasses.write(",".join(l) + "\n")



exit(0)

