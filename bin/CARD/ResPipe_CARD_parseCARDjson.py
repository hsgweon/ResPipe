#!/usr/bin/env python

import argparse, json, sys
parser = argparse.ArgumentParser("")
parser.add_argument("--card_json",
                    action = "store", 
                    dest = "card_json", 
                    metavar = "card_json",
                    help = "[REQUIRED]",
                    required = True)
parser.add_argument("--aro_obo",
                    action = "store", 
                    dest = "aro_obo", 
                    metavar = "aro_obo",
                    help = "[REQUIRED]",
                    required = True)
parser.add_argument("-o",
                    action = "store", 
                    dest = "output", 
                    metavar = "output",
                    help = "[REQUIRED]",
                    required = True)
options = parser.parse_args()


# Deal with aro.obo
# The first part is to extrapolate AMR gene -> "confers_resistance_to" and "confers_resistance_to_drug". This was initially developed for Olga Auguet's GCE project.
#
# Creating dictionaries: 
# ARO_OBO_confers_resistance_to[ARO] = a list of 'confers_resistance_to'
# ARO_OBO_confers_resistance_to_drug[ARO] = a list of 'confers_resistance_to_drug'

aro_obo = open(options.aro_obo)

ARO_OBO_id = ""
ARO_OBO_confers_resistance_to = {}
ARO_OBO_confers_resistance_to_drug = {}

for line in aro_obo:

	# [Typedef] are found at the end of the file. Essentially an EOF.
	if line.startswith("[Typedef]"): 
		break

	if line.startswith("id: ARO:"):
		ARO_OBO_id = line.split("id: ")[1].strip()
		ARO_OBO_confers_resistance_to[ARO_OBO_id] = []
		ARO_OBO_confers_resistance_to_drug[ARO_OBO_id] = []
	elif line.startswith("relationship: confers_resistance_to ARO:"):
		ARO_OBO_confers_resistance_to[ARO_OBO_id].append(line.split("relationship: confers_resistance_to ARO:")[1].split("!")[0].strip())
	elif line.startswith("relationship: confers_resistance_to_drug ARO:"):
		ARO_OBO_confers_resistance_to_drug[ARO_OBO_id].append(line.split("relationship: confers_resistance_to_drug ARO:")[1].split("!")[0].strip())
	elif line.startswith("[") and ARO_OBO_id != "":
		# print(ARO_OBO_id)
		# print(ARO_OBO_confers_resistance_to[ARO_OBO_id])
		# print(ARO_OBO_confers_resistance_to_drug[ARO_OBO_id])
		ARO_OBO_id = ""
#print(ARO_OBO_confers_resistance_to["ARO:3000094"])



# Load CARD JSON
card_file = open(options.card_json)
card = json.load(card_file)
card_file.close()

outfile = open(options.output, "w")

ARO2META = {}

AMR_Gene_Family = []
Drug_Class = []
Resistance_Mechanism = []
Antibiotic = []
Efflux_Component = []
Efflux_Regulator = []

for i in card:

	entry = card[i]

	try:
		ARO = "ARO:" + entry["ARO_accession"].rstrip()
		ARO_name = entry["ARO_name"].rstrip()
		CVTERMID = entry["ARO_id"].rstrip() # This is an ID used in webbrowser. Don't entirely certain why this is different from ARO... Added in case we want to do some text-mining from CARD webpage.

		del AMR_Gene_Family[:]
		del Drug_Class[:]
		del Resistance_Mechanism[:]
		del Antibiotic[:]
		del Efflux_Component[:]
		del Efflux_Regulator[:]

		for i in entry["ARO_category"]:

			caa = entry["ARO_category"][i]["category_aro_accession"].replace(" ", "_")
			can = entry["ARO_category"][i]["category_aro_name"].replace(" ", "_")
			cacn = entry["ARO_category"][i]["category_aro_class_name"].replace(" ", "_")

			if ";" in can:
				print("Illegal Character detected.")
				exit(1)

			if cacn == "AMR_Gene_Family":
				AMR_Gene_Family.append(can)
			elif cacn == "Resistance_Mechanism":
				Resistance_Mechanism.append(can)
			elif cacn == "Drug_Class":
				if caa in ARO_OBO_confers_resistance_to[ARO]:
					can = "confers_resistance_to__" + can
				Drug_Class.append(can)
			elif cacn == "Antibiotic":
				if caa in ARO_OBO_confers_resistance_to_drug[ARO]:
					can = "confers_resistance_to_drug__" + can
				Antibiotic.append(can)
			elif cacn == "Efflux_Component":
				Efflux_Component.append(can)
			elif cacn == "Efflux_Regulator":
				Efflux_Regulator.append(can) 


		# NB!!! We need to deal with multiple AMR Gene Family entries.
		# I am going to redefine AMR Gene Family for the sake of ResPipe - Gene family containing
		# E.g. 
		# "glycopeptide_resistance_gene_cluster;XXXXXX" will be renamed to just "glycopeptide_resistance_gene_cluster"
		# Entries with 'Efflux_Component' will be given 'efflux_pump_complex_subunit' to AMR Gene Family
		# Entries with 'Efflux_Regulator' will be given 'efflux_pump_regulator' to AMR Gene Family
		#
		# Ensure there is a single entry for AMR Gene Family.
		if "glycopeptide_resistance_gene_cluster" in AMR_Gene_Family:
			AMR_Gene_Family = ['glycopeptide_resistance_gene_cluster']
		if (len(AMR_Gene_Family) > 1) and (len(Efflux_Component) != 0) and (len(Efflux_Regulator) == 0):
			AMR_Gene_Family = ['efflux_pump_complex_subunit']
		if (len(AMR_Gene_Family) > 1) and (len(Efflux_Component) != 0) and (len(Efflux_Regulator) != 0):
			AMR_Gene_Family = ['efflux_pump_regulator']
		if "fluoroquinolone_resistant_parC" in AMR_Gene_Family:
			AMR_Gene_Family = ['fluoroquinolone_resistant_parC']
		if "AAC(3)" in AMR_Gene_Family:
			AMR_Gene_Family = ['AAC(3)']
		if "AAC(6')" in AMR_Gene_Family:
			AMR_Gene_Family = ['AAC(6\')']
		if "class_C_LRA_beta-lactamase" in AMR_Gene_Family and "class_D_LRA_beta-lactamase" in AMR_Gene_Family:
			AMR_Gene_Family = ['class_C_and_D_LRA_beta-lactamase']
		if "UhpA" in AMR_Gene_Family and "UhpT" in AMR_Gene_Family:
			AMR_Gene_Family = ['UhpA_UhpT']
		if "23S_rRNA_with_mutation_conferring_resistance_to_macrolide_antibiotics" in AMR_Gene_Family and "23S_rRNA_with_mutation_conferring_resistance_to_streptogramins_antibiotics" in AMR_Gene_Family:
			AMR_Gene_Family = ['23S_rRNA_with_mutation_conferring_resistance_to_macrolide_and_streptogramins_antibiotics']
		if "aminocoumarin_resistant_parY" in AMR_Gene_Family and "aminocoumarin_self_resistant_parY" in AMR_Gene_Family:
			AMR_Gene_Family = ['aminocoumarin_self_resistant_parY']
		if "elfamycin_resistant_EF-Tu" in AMR_Gene_Family and "kirromycin_self_resistant_EF-Tu" in AMR_Gene_Family:
			AMR_Gene_Family = ['elfamycin_resistant_EF-Tu']

		# Sort alphabetically:
		AMR_Gene_Family = sorted(AMR_Gene_Family)
		Drug_Class = sorted(Drug_Class)
		Resistance_Mechanism = sorted(Resistance_Mechanism)
		Efflux_Component = sorted(Efflux_Component)
		Efflux_Regulator = sorted(Efflux_Regulator)

		# If empty:
		if len(AMR_Gene_Family) == 0:
			AMR_Gene_Family.append("Unassigned")
		if len(Drug_Class) == 0:
			Drug_Class.append("Unassigned")
		if len(Resistance_Mechanism) == 0:
			Resistance_Mechanism.append("Unassigned")
		if len(Antibiotic) == 0:
			Antibiotic.append("Unassigned")
		if len(Efflux_Component) == 0:
			Efflux_Component.append("Unassigned")
		if len(Efflux_Regulator) == 0:
			Efflux_Regulator.append("Unassigned")

		meta = ARO_name + "\t" + \
				";".join(AMR_Gene_Family) + "\t" + \
				";".join(Drug_Class) + "\t" + \
				";".join(Resistance_Mechanism) + "\t" + \
				";".join(Antibiotic) + "\t" + \
				";".join(Efflux_Component) + "\t" + \
				";".join(Efflux_Regulator) + "\t" + \
				CVTERMID

		# Check if there are two different versions of the same ARO.
		if ARO in ARO2META:
			if meta != ARO2META[ARO]:
				print("Problem: There are two different versions of the same ARO.")
				print(meta)
				print(ARO2META[ARO])
				exit(1)

		ARO2META[ARO] = meta

		# Ensure there is a single entry for AMR Gene Family. (do this for every new release of CARD)
		if len(AMR_Gene_Family) > 1:
			print(ARO)
			print(AMR_Gene_Family)
			print(Efflux_Component)
			print(Efflux_Regulator)
			print("")
			exit(1)

	except (KeyError, TypeError):
		continue

outfile.write("#ARO" + "\t" + "ARO_name" + "\t" + "AMR_Gene_Family" + "\t" + "Drug_Class" + "\t" + "Resistance_Mechanism" + "\t" + "Antibiotic" + "\t" + "Efflux_Component" + "\t" + "Efflux_Regulator" + "\t" + "CVTERMID" + "\n")

for key, value in ARO2META.items():
	outfile.write(key + "\t" + value + "\n")

exit(0)


