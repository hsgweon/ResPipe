#!/usr/bin/env python
import argparse, sys
parser = argparse.ArgumentParser("")
parser.add_argument("-i",
                    action = "store", 
                    dest = "input", 
                    metavar = "input",
                    help = "[REQUIRED]",
                    required = True)
parser.add_argument("--ARO2META",
                    action = "store", 
                    dest = "ARO2META", 
                    metavar = "ARO2META",
                    help = "[REQUIRED]",
                    required = True)
parser.add_argument("-o",
                    action = "store", 
                    dest = "output", 
                    metavar = "output",
                    help = "[REQUIRED]",
                    required = True)
parser.add_argument("-d",
                    action = "store", 
                    dest = "data", 
                    metavar = "data",
                    help = "[REQUIRED]",
                    required = True)
options = parser.parse_args()

dict_ARO2META = {}
with open(options.ARO2META, "r") as infile:
    for line in infile:
        if not line.startswith("#"):
            dict_ARO2META[line.split()[0].rstrip()] = line.rstrip()

outfile = open(options.output, "w")
datafile = open(options.data, "w")
datafile.write("#ResPipeUI" + "\t" + \
                "ARO_accession" + "\t" + \
                "ARO_name" + "\t" + \
                "AMR_Gene_Family" + "\t" + \
                "Drug_Class" + "\t" + \
                "Resistance_Mechanism" + "\t" + \
                "Antibiotic" + "\t" + \
                "Efflux_Component" + "\t" + \
                "Efflux_Regulator" + "\t" + \
                "CVTERMID" + "\t" + \
                "gb" + "\t" + \
                "Species" + "\t" + \
                "SeqLength" + "\n")

index = 1
ResPipeUIs = []

from Bio import SeqIO

for record in SeqIO.parse(options.input, "fasta"):

    header = record.description

    ARO = header.split("|")[4]
    gbi = header.split("|")[1]
    ARO_name = header.split("|")[5].split("[")[0].strip()
    species = header.split("|")[5].split("[")[1].split("]")[0].strip()
    seqlength = len(record.seq)


    # Check header integrity
    if not ARO.startswith("ARO:") or not header.startswith("gb"):
    	print(header)
    	exit(1)

    # Also check to ensure the sequence is in ARO2CVTERMID:
    if not ARO in dict_ARO2META:
        print("Sequence not in ARO2CVTERMID")
        print(ARO)
        exit(1)

    # ResPipeUI = "RP_" + "{0:06d}".format(index)
    ResPipeUI = ARO[4:] + "_" + gbi

    # Check to ensure that there are no duplicate ResPipeUIs
    if ResPipeUI in ResPipeUIs:
        print("Non-unique ResPipeUI: " + ResPipeUI)
        exit(1)
    else:
        ResPipeUIs.append(ResPipeUI)

    outfile.write(">" + ResPipeUI + "\n")
    outfile.write(str(record.seq) + "\n")

    datafile.write(ResPipeUI + "\t" + dict_ARO2META[ARO] + "\t" + gbi + "\t" + species + "\t" + str(seqlength) + "\n")

    index += 1

outfile.close()
datafile.close()
