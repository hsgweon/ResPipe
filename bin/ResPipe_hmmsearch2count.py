#!/usr/bin/env python
#
#  ____             ____   _              
# |  _ \  ___  ___ |  _ \ (_) _ __    ___ 
# | |_) |/ _ \/ __|| |_) || || '_ \  / _ \
# |  _ <|  __/\__ \|  __/ | || |_) ||  __/
# |_| \_\\___||___/|_|    |_|| .__/  \___|
#                             |_|
#
# ResPipe
# Copyright (C) 2017 Hyun Soon Gweon
# 
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.
#
# For any bugs or problems found, please contact us at:
# H. Soon Gweon <h.s.gweon@reading.ac.uk>

import argparse, glob, pandas as pd
from collections import OrderedDict
parser = argparse.ArgumentParser("")
parser.add_argument("-i",
                    action = "store", 
                    dest = "infile", 
                    metavar = "infile",
                    help = "[REQUIRED]",
                    required = True)
parser.add_argument("-o",
                    action = "store",
                    dest = "outfile",
                    metavar = "outfile",
                    help = "[REQUIRED]",
                    required = True)
options = parser.parse_args()

gene_dict = {"dnaG": 0, "frr": 0, "infC": 0, "nusA": 0, "pgk": 0, "pyrG": 0, "rplA": 0, "rplB": 0, "rplC": 0, "rplD": 0,
             "rplE": 0, "rplF": 0, "rplK": 0, "rplL": 0, "rplM": 0, "rplN": 0, "rplP": 0, "rplS": 0, "rplT": 0, "rpmA": 0, 
             "rpoB": 0, "rpsB": 0, "rpsC": 0, "rpsE": 0, "rpsI": 0, "rpsJ": 0, "rpsK": 0, "rpsM": 0, "rpsS": 0, "smpB": 0, "tsf": 0}

with open(options.infile, "r") as infile:
    for line in infile:
        if line.startswith("#"):
            continue
        gene = line.split()[1].split("_")[0]
        gene_dict[gene] += 1

with open(options.outfile, "w") as outfile:
    # outfile.write(options.infile.split(".hmmsearch")[0] + "\n")
    for i in sorted(gene_dict):
        outfile.write(i + "\t" + str(gene_dict[i]) + "\n")
