#!/usr/bin/env nextflow
/*
 ____             ____   _              
|  _ \  ___  ___ |  _ \ (_) _ __    ___ 
| |_) |/ _ \/ __|| |_) || || '_ \  / _ \
|  _ <|  __/\__ \|  __/ | || |_) ||  __/
|_| \_\\___||___/|_|    |_|| .__/  \___|
                            |_|          

ResPipe
Copyright (C) 2018 Hyun Soon Gweon
        
This script is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This script is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this script. If not, see <http://www.gnu.org/licenses/>.

For any bugs or problems found, please contact us at:
H. Soon Gweon <h.s.gweon@reading.ac.uk>

*/

// Version and help
params.version = false
params.help = false

// Configurable variables
params.in = false
params.out = "ResPipe_results"

params.listfile	= false

params.func = false
params.C_only = false // CARD only
params.I_only = false // ISfinder only
params.N_only = false // NCBI Enterobacteriaceae Plasmid only
params.M_only = false // Misc reference only

params.thermus = false

params.taxo = false
params.krakenOnly = false
params.centrifugeOnly = false
params.extract16S = false

params.countSCG = false

// Configurable variables that may be set within config profiles
params.kraken_db = false
params.braken_distrib = false
params.centrifuge_abv = false
params.kraken_ramdisk = false
params.kraken_preload = false

// Print version if asked
if (params.version) {
	System.out.println("ResPipe - Version: $VERSION ($TIMESTAMP)")
	exit 1
}

// Print help if asked
if (params.help) {
	System.out.println("")
	System.out.println("")
	System.out.println("Usage:")
	System.out.println("   nextflow run ResPipe.nf --in FASTQ_DIR --listfile LISTFILE")
	System.out.println("                [options] [--func]")
	System.out.println("                [options] [--taxo   --kraken_preload|--kraken_ramdisk]")
	System.out.println("                [options] [--thermus HB27|HB8]")
        System.out.println("                [options] [--extract16S]")
        System.out.println("                [options] [--countSCG]")
	System.out.println("                [options] [-profile PROFILE]")
	System.out.println("")
	System.out.println("")
	exit 1
}


/////////////////////
// Validate inputs //
/////////////////////

if (!params["in"]) {
  println ""
  println "Error: Please specify the location of prepped files with \"--in\". See README for more details."
  println ""
  exit 1
} 

if (params["listfile"]) {
   if( !file(params.listfile).exists() ) exit 1, "File not found: ${params.listfile}"
} else {
   exit 1, "Please specify the location of listfile files with \"--listfile\". See README for more details."
}


//////////
// taxo //
//////////

runCentrifuge = false
runKraken = false

if (params["taxo"]) {

   runCentrifuge = true
   runKraken = true

   if (params["centrifugeOnly"]) {
      runKraken = false
   }

   if (params["krakenOnly"]) {
      runCentrifuge = false
   }

}

if (params["taxo"]) {
    
    if (runKraken) {
    
        // Check Kraken DB locations are set
        if (!params["kraken_db"]) {
            exit 1, "Please specify the location of kraken databse with \"--kraken_db\". See README for more details."
        }

        if (!params["braken_distrib"]) {
            exit 1, "Please specify the location of braken distribution with \"--braken_distrib\". See README for more details."
        }

        // Need to select a way to run kraken
        if (!params["kraken_preload"]) {
            if (!params["kraken_ramdisk"]) {
                exit 1, "Please select either \"--kraken_preload\" or \"--kraken_ramdisk\".. See README for more details."
            }
        }
        else if (params["kraken_ramdisk"]) {
            exit 1, "Cannot use \"--kraken_preload\" and \"--kraken_ramdisk\" simultaneously. See README for more details."
        }

    }

    if (runCentrifuge) {

        // Check contrifuge DB location is set
        if (!params["centrifuge_abv"]) {
            exit 1, "Please specify the location of centrifuge database with \"--centrifuge_abv\". See README for more details."
        }

    } 

}


/////////////////////////////
// Log dependency versions //
/////////////////////////////

process retrieve_dependencies_versions_taxo {

    publishDir "${params.out}/taxo_dependencies", mode: 'copy'

    output:
    file "*.txt"

    when:
    params.taxo == true

    script:
    """
    echo $VERSION > ResPipe.txt
    kraken2 -v > kraken.txt
    est_abundance.py -h > est_abundance.txt
    """
}

process retrieve_dependencies_versions_func {

    publishDir "${params.out}/func_dependencies", mode: 'copy'

    output:
    file "*.txt"

    when:
    params.func == true

    script:
    """
    echo $VERSION > ResPipe.txt
    samtools --version > v_samtools.txt
    bedtools --version > v_bedtools.txt
    bbversion.sh > v_bbmap.txt
    """
}

process retrieve_dependencies_versions_extract16S {

    publishDir "${params.out}/extract16S_dependencies", mode: 'copy'

    output:
    file "*.txt"

    when:
    params.extract16S == true

    script:
    """
    echo $VERSION > ResPipe.txt
    nhmmer -h > v_nhmmer.txt
    """
}

process retrieve_dependencies_versions_countSCG {

    publishDir "${params.out}/countSCG_dependencies", mode: 'copy'

    output:
    file "*.txt"

    when:
    params.countSCG == true

    script:
    """
    echo $VERSION > ResPipe.txt
    orfm -v > v_orfm.txt
    diamond version > v_diamond.txt
    """
}


//////////////////////
// Initial channels //
//////////////////////

Channel
    .fromPath("${params.in}/*.fq")
    .map { file -> [file.baseName.split(/_r\d(_unpaired_|_val_)/)[0], file] }
    //.subscribe { println "vvalue: $it" }
    .set { c_files }

Channel
    .fromPath(params.listfile)
    .splitCsv(sep: "\t")
    .map { row -> [ row[0], row[1] ] }
    //.subscribe { println "value: $it" }
    .set { c_listfile }

c_listfile.cross( c_files )
    .map { it -> tuple( it[0][1], it[1][1] ) }
    .groupTuple(by:0)
    //.subscribe { println "value: $it" }
    .into { p_map_reads; p_run_kraken2; p_run_centrifuge; p_extract_16S; p_extract_SCG }



////////////
// Kraken //
////////////

process run_kraken2 {

    tag { "sample: ${sample_id}, CPUs: ${task.cpus}, paired_f: ${paired_f}, paired_r: ${paired_r}" }

    label 'moderateMemory'
    label 'krakenCPU'

    publishDir "${params.out}/taxo/",
       mode: 'copy',
       saveAs: { filename ->
          if( filename.indexOf(".kreport2") > 0 )
	     return "kraken2/$filename"
	  //else if( filename.indexOf(".kraken2") > 0)
	  //   return "kraken2/$filename"
	  else if( filename.indexOf(".bracken") > 0)
	     return "bracken/$filename"
	  else
	     return null
    }

    input:
    set val(sample_id), file(files) from p_run_kraken2

    output:
    set file("${sample_id}.kraken2"), file("${sample_id}.kreport2"), file("${sample_id}.bracken")

    when:
    runKraken == true
    
    script:
    kraken_db = params.kraken_db
    braken_distrib = params.braken_distrib
    
    paired_f = "${files}".tokenize(" ").findAll { item -> item.contains("r1_val_1") }.sort().join(" ")
    paired_r = "${files}".tokenize(" ").findAll { item -> item.contains("r2_val_2") }.sort().join(" ")
    single   = "${files}".tokenize(" ").findAll { item -> item.contains("unpaired") }.sort().join(" ")

    if (params.kraken_ramdisk)
        """
        if [ ! -d ${params.kraken_ramdisk}/krakenDB ]; then
            mkdir ${params.kraken_ramdisk}/krakenDB/
        fi
        cp -r ${kraken_db}/taxonomy ${params.kraken_ramdisk}/krakenDB/
        cp -r ${kraken_db}/database.idx ${params.kraken_ramdisk}/krakenDB/
        cp -r ${kraken_db}/database.kdb ${params.kraken_ramdisk}/krakenDB/

	touch ${sample_id}_p.kraken2
        touch ${sample_id}_s.kraken2
	
	kraken2 --db $kraken_db --output ${sample_id}_p.kraken2 --paired --threads $task.cpus --report-zero-counts --memory-mapping --report ${sample_id}_p.kreport2 <(cat $paired_f) <(cat $paired_r)
        kraken2 --db $kraken_db --output ${sample_id}_s.kraken2          --threads $task.cpus --report-zero-counts --memory-mapping --report ${sample_id}_s.kreport2 <(cat $single)

        cat ${sample_id}_p.kraken2 ${sample_id}_s.kraken2 > ${sample_id}.kraken2

        ResPipe_merge_kreports.py -i ${sample_id}_p.kreport2,${sample_id}_s.kreport2 -o ${sample_id}.kreport2

        bracken -d $kraken_db -i ${sample_id}.kreport2 -o ${sample_id}.bracken -r 100 -l 'S' -t $task.cpus
        est_abundance.py -i ${sample_id}.kreport2 -k $braken_distrib -t $task.cpus -o ${sample_id}.bracken

        """
    else if(params.kraken_preload)
        """

	touch ${sample_id}_p.kraken2
	touch ${sample_id}_s.kraken2

	kraken2 --db $kraken_db --output ${sample_id}_p.kraken2 --paired --threads $task.cpus --report-zero-counts --memory-mapping --report ${sample_id}_p.kreport2 <(cat $paired_f) <(cat $paired_r)
	kraken2 --db $kraken_db --output ${sample_id}_s.kraken2          --threads $task.cpus --report-zero-counts --memory-mapping --report ${sample_id}_s.kreport2 <(cat $single)

	cat ${sample_id}_p.kraken2 ${sample_id}_s.kraken2 > ${sample_id}.kraken2

	ResPipe_merge_kreports.py -i ${sample_id}_p.kreport2,${sample_id}_s.kreport2 -o ${sample_id}.kreport2

	bracken -d $kraken_db -i ${sample_id}.kreport2 -o ${sample_id}.bracken -r 100 -l 'S' -t $task.cpus
	est_abundance.py -i ${sample_id}.kreport2 -k $braken_distrib -t $task.cpus -o ${sample_id}.bracken
        """
}


////////////////
// Centrifuge //
////////////////

process run_centrifuge {

    tag { "sample: ${sample_id}, CPUs: ${task.cpus}, paired_f: ${paired_f}, paired_r: ${paired_r}" }

    label 'highMemory'
    label 'centrifugeCPU'
    
    publishDir "${params.out}/taxo/centrifuge/", mode: 'copy'

    input:
    set val(sample_id), file(files) from p_run_centrifuge

    output:
    file("${sample_id}.centrifuge.report")
    
    when:
    runCentrifuge == true

    script:
    centrifuge_abv = params.centrifuge_abv

    paired_f = "${files}".tokenize(" ").findAll { item -> item.contains("r1_val_1") }.sort().join(" ")
    paired_r = "${files}".tokenize(" ").findAll { item -> item.contains("r2_val_2") }.sort().join(" ")
    single   = "${files}".tokenize(" ").findAll { item -> item.contains("unpaired") }.sort().join(" ")

    """
    centrifuge -p $task.cpus -q -x $centrifuge_abv -1 <(cat $paired_f) -2 <(cat $paired_r) -U <(cat $single) -S ${sample_id}.centrifuge --report-file ${sample_id}.centrifugeSummary
    centrifuge-kreport -x $centrifuge_abv ${sample_id}.centrifuge > ${sample_id}.centrifuge.report
    """
    
}



//////////
// func //
//////////

/////////////////////////////
// Map reads to genes etc. //
/////////////////////////////

ref_CARD_metadata = "${baseDir}/data/ResPipe_CARD-3.0.3.meta.tsv"

ref_CARD = "${baseDir}/data/ResPipe_CARD-3.0.3.fasta"
ref_ISfinder = "${baseDir}/data/ResPipe_ISfinder.fasta"
ref_NCBIEnterobacterPlasmids = "${baseDir}/data/ResPipe_NCBIEnterobacterPlasmids.fasta"
ref_CustomFasta = "${baseDir}/data/CustomFasta"

refs = [ "CARD"                      : ref_CARD, 
         "ISfinder"                  : ref_ISfinder, 
         "NCBIEnterobacterPlasmids"  : ref_NCBIEnterobacterPlasmids ] // Don't put "_" in the name of the ref database!

if (params["C_only"] == true) {
    refs = ["CARD" : ref_CARD]
} else if (params["I_only"] == true) {
    refs = ["ISfinder" : ref_ISfinder]
} else if (params["N_only"] == true) {
    refs = ["NCBIEnterobacterPlasmids" : ref_NCBIEnterobacterPlasmids]
} else if (params["M_only"] == true) {
    refs = [ "CustomFasta" : ref_CustomFasta ]
}


ref_ThermusThermophilus = false
mask_ThermusThermophilus = false

if (params["thermus"] == "HB27"){
    ref_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB27__NC_005835.fasta"
    mask_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB27__NC_005835_mask.bed"
    refs << ["ThermusThermophilus" : ref_ThermusThermophilus]
} else if (params["thermus"] == "HB8"){
    ref_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB8__NC_006461.fasta"
    mask_ThermusThermophilus = "${baseDir}/data/Thermus_thermophilus_HB8__NC_006461_mask.bed"
    refs << ["ThermusThermophilus" : ref_ThermusThermophilus]
} else if (params["thermus"] == false) {
    ref_ThermusThermophilus = false
    mask_ThermusThermophilus = false
} else {
    exit 1, "Unrecognised Thermus strain. Please select either \"--thermus HB27\" or \"--thermus HB8\""
}


process map_reads {

    tag "sample: ${sample_id}, ref: ${ref}, CPUs: ${task.cpus}, files: ${in_files}"

    label "lowMemory"
    label "parallelisable"

    //publishDir "${params.out}/func/1_sams_original_mapping", mode: 'copy'

    input:
    set val(sample_id), file(files) from p_map_reads
    each ref from refs.keySet()

    output:
    file "${ref}_${sample_id}.sam" into p_appraise_sams

    when:
    params.func == true

    script:
    in_files = "${files}"
    out_file = "${ref}_${sample_id}.sam"
    ref_path = refs[ref]

    """
    cat $in_files | bbmapskimmer.sh -Xmx${task.memory.giga}g in=stdin.fq outm=$out_file ref=$ref_path ambig=all saa=f sam=1.3 semiperfectmode=t int=f t=${task.cpus} nodisk
    """

}


///////////////////
// The Appraisal //
/////////////////// Only at this stage, BEDTOOLS INTERSECT called for TT

process appraise_sams {

    maxForks 3

    tag "ref_sample: ${ref_sample}"
    label "moderateMemory"

    //publishDir "${params.out}/func/3_count_for_each_sample", mode: 'copy'

    input:
    file ref_sample from p_appraise_sams

    output:
    file("*.tsv") into c_create_tables

    script:
    filename = "${ref_sample}".tokenize(".")[0]
    ref = "${ref_sample}".tokenize("_")[0]
    ref_path = refs[ref]

    if ( ref == "CARD" )
        """
        ResPipe_SAM2REF.py             -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path}
        ResPipe_SAM2REF_hierachical.py -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path} --meta_data ${ref_CARD_metadata}
        rm -f ${filename}.sam
        """
    else if ( ref == "ISfinder" || ref == "NCBIEnterobacterPlasmids")
    	"""
        ResPipe_SAM2REF.py -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path}
        rm -f ${filename}.sam
        """
    else if ( ref == "ThermusThermophilus" )
    	"""
        samtools view -b -S -o ${filename}_unmasked.bam ${filename}.sam
        bedtools intersect -a ${filename}_unmasked.bam -b $mask_ThermusThermophilus -v > ${filename}_masked.bam
        samtools view -h -o ${filename}_masked.sam ${filename}_masked.bam
        ResPipe_SAM2REF.py -i ${filename}_masked.sam -o ${filename} --ref_fasta ${ref_path}
        rm -f ${filename}_unmasked.bam ${filename}_masked.bam ${filename}_masked.sam
        """
    else if ( ref == "CustomFasta" )
    	"""
        ResPipe_SAM2REF.py -i ${filename}.sam -o ${filename} --ref_fasta ${ref_path}
        rm -f ${filename}.sam
        """
    else
	error "Invalid alignment ref: ${mode}"

}


/////////////////////////////////////////////////////
// Pull all results and combine into one big table //
/////////////////////////////////////////////////////

process create_tables {

    publishDir "${params.out}/func", mode: 'copy'

    input:
    file(files) from c_create_tables.collect()

    output:
    file("*.tsv")

    script:
    """
    ResPipe_combineTSVs.py --glob "CARD_*_read_count.tsv" -o "CARD_read_count.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_read_count_specific.tsv" -o "CARD_read_count_specific.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_lateral_coverage.tsv" -o "CARD_lateral_coverage.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_lateral_coverage_specific.tsv" -o "CARD_lateral_coverage_specific.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_read_lengths.tsv" -o "CARD_read_lengths.tsv"
    ResPipe_combineTSVs.py --glob "CARD_*_read_lengths_specific.tsv" -o "CARD_read_lengths_specific.tsv"

    ResPipe_combineTSVs.py --glob "CARD_*_read_count_hierachical.tsv" -o "CARD_read_count_hierachical.tsv"

    ResPipe_combineTSVs.py --glob "ISfinder_*_read_count.tsv" -o "ISfinder_read_count.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_read_count_specific.tsv" -o "ISfinder_read_count_specific.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_lateral_coverage.tsv" -o "ISfinder_lateral_coverage.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_lateral_coverage_specific.tsv" -o "ISfinder_lateral_coverage_specific.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_read_lengths.tsv" -o "ISfinder_read_lengths.tsv"
    ResPipe_combineTSVs.py --glob "ISfinder_*_read_lengths_specific.tsv" -o "ISfinder_read_lengths_specific.tsv"

    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_count.tsv" -o "NCBIEnterobacterPlasmids_read_count.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_count_specific.tsv" -o "NCBIEnterobacterPlasmids_read_count_specific.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_lateral_coverage.tsv" -o "NCBIEnterobacterPlasmids_lateral_coverage.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_lateral_coverage_specific.tsv" -o "NCBIEnterobacterPlasmids_lateral_coverage_specific.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_lengths.tsv" -o "NCBIEnterobacterPlasmids_read_lengths.tsv"
    ResPipe_combineTSVs.py --glob "NCBIEnterobacterPlasmids_*_read_lengths_specific.tsv" -o "NCBIEnterobacterPlasmids_read_lengths_specific.tsv"

    ResPipe_combineTSVs.py --glob "ThermusThermophilus_*_read_count.tsv" -o "ThermusThermophilus_read_count.tsv"
    ResPipe_combineTSVs.py --glob "ThermusThermophilus_*_lateral_coverage.tsv" -o "ThermusThermophilus_lateral_coverage.tsv"

    ResPipe_combineTSVs.py --glob "CustomFasta_*_read_count.tsv" -o "CustomFasta_read_count.tsv"
    ResPipe_combineTSVs.py --glob "CustomFasta_*_lateral_coverage.tsv" -o "CustomFasta_lateral_coverage.tsv"
    """

}


//////////////////////////////////////
// 16S retrieval and classification //
//////////////////////////////////////

profile_bacteria_16S = "${baseDir}/data/profile_bacteria_16S.hmm"

process extract_16S {

    tag { "files_f: ${files_f} | files_r: ${files_r}" }
    label 'parallelisable'
    label 'moderateMemory'

    publishDir "${params.out}/extract16S/", mode: 'copy'

    input:
    set val(sample_id), file(files) from p_extract_16S

    output:
    file "${sample_id}_16S.fasta"

    when:
    params.extract16S == true

    script:
    files_f = "${files}".tokenize(" ").findAll { item -> item.contains("_r1_") }.sort().join(" ")
    files_r = "${files}".tokenize(" ").findAll { item -> item.contains("_r2_") }.sort().join(" ")

    """
    cat <( cat ${files_f} | seqtk seq -A ) <( cat ${files_r} | seqtk seq -rA ) | nhmmer -E 1e-10 --cpu ${task.cpus} --w_length 2000 -o ${sample_id}.hmmsearch.out --tblout ${sample_id}.hmmsearch -A ${sample_id}.stk $profile_bacteria_16S -
    esl-reformat -d afa ${sample_id}.stk | sed '/^[^>]/s/[-.]//g' | seqtk cutN - -n 50 > ${sample_id}_16S.fasta
    """

}


//////////////////////////////////
// Extract 31 Single Copy Genes //
//////////////////////////////////

db_SCG = "${baseDir}/data/31SCG.dmnd"

process extract_SingleCopyGenes {

    tag { "sample: ${sample_id}, CPUs: ${task.cpus}, files: ${files}" }
    label 'highMemory'

    publishDir "${params.out}/countSCG/", mode: 'copy'

    input:
    set val(sample_id), file(files) from p_extract_SCG

    output:
    set file("${sample_id}.m8"), file("${sample_id}.count")

    when:
    params.countSCG == true

    script:
    """
    cat $files | orfm | diamond blastp -e 1e-3 -k 1 -c 1 -p 1 -d $db_SCG -o ${sample_id}.m8
    ResPipe_hmmsearch2count.py -i ${sample_id}.m8 -o ${sample_id}.count
    """

}

workflow.onComplete {

    println "Completed at: $workflow.complete"
    println "Execution status: ${ workflow.success ? 'OK' : 'failed' }"

    def msg = """\
        Pipeline execution summary
        ---------------------------
        Completed at: ${workflow.complete}
        Duration    : ${workflow.duration}
        Success     : ${workflow.success}
	workDir     : ${workflow.workDir}
        exit status : ${workflow.exitStatus}
	"""
	.stripIndent()

    sendMail(to: 'hyugwe@ceh.ac.uk', subject: 'ResPipe execution', body: msg)

}
