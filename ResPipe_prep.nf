#!/usr/bin/env nextflow
/*
 ____             ____   _              
|  _ \  ___  ___ |  _ \ (_) _ __    ___ 
| |_) |/ _ \/ __|| |_) || || '_ \  / _ \
|  _ <|  __/\__ \|  __/ | || |_) ||  __/
|_| \_\\___||___/|_|    |_|| .__/  \___|
                            |_|          

ResPipe
Copyright (C) 2018 Hyun Soon Gweon
        
This script is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This script is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this script. If not, see <http://www.gnu.org/licenses/>.

For any bugs or problems found, please contact us at:
H. Soon Gweon <h.s.gweon@reading.ac.uk>

*/

// Version and help
params.version = false
params.help = false

// Configurable Variables
params.in = false
params.bam = false
params.readPattern = "*_r{1,2}.fastq.gz"
params.fastqOut = "fastqs"
params.prepOut = "ResPipe_prep"

// Print help if asked
if (params.help) {
	System.out.println("")
	System.out.println("")
	System.out.println("Usage: ")
	System.out.println("   nextflow run ResPipe_prep.nf --in DIR [--readPattern \"PATTERN\"]  [--bam]")
	System.out.println("                [options] [--out OUTPUT_DIR] [-profile PROFILE]")
	System.out.println("")
	System.out.println("")
    exit 1
}

// Validate inputs
if (!params["in"]) {
  println ""
  println "Error: Please specify the location of BAM or FASTQ files with \"--in\". See README for more details."
  println ""
  exit 1
}

if (params["bam"]) {
    Channel.fromPath( params.in + "/*.bam" )
           .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
           .set { bams }

    process bam2fastq {
      
      tag { bam }

      publishDir params.fastqOut, mode: 'copy'

      input:
      file bam from bams

      output:
      set val(bam_tag), file("*.fastq.gz") into p_fastqc, p_qualityFilter 

      shell:
      bam_tag = bam.baseName

      """
      samtools collate -Ou -n 128 --threads 1 $bam tmp-prefix | samtools fastq - -1 ${bam_tag}_r1.fastq.gz -2 ${bam_tag}_r2.fastq.gz -N
      """
    }
} else {
    Channel
        .fromFilePairs("${params.in}" + "/" + params.readPattern)
        .ifEmpty { error "Cannot find any reads matching: ${params.readPattern}" }
        .set { fastqs }

    process checkFastqForwardReverse {
      
        tag { "${sample_name} - checkFastq" }

        input:
        set val(sample_name), file(files) from fastqs

        output:
        set val(sample_name), file('checked/*.fq.gz')  into p_qualityFilter 

        script:
        """
        mkdir checked
        ResPipe_prep_checkFWandRE.py             -i ${files[0]} -o checked/${sample_name}_r1.fq.gz -r 1
        ResPipe_prep_checkFWandRE.py             -i ${files[1]} -o checked/${sample_name}_r2.fq.gz -r 2
        """
    }

}

// Print version if asked
if (params.version) {
	System.out.println("ResPipe - Version: $VERSION ($TIMESTAMP)")
	exit 1
}


/////////////////////////////
// Log dependency versions //
/////////////////////////////

process retrieve_dependencies_versions {

    publishDir "${params.prepOut}", mode: 'copy'

    output:
    file "*.txt" into software_versions_yaml

    script:
    """
    echo $VERSION > v_ResPipe.txt
    fastqc --version > v_fastqc.txt
    cutadapt --version > v_cutadapt.txt
    trim_galore --version > v_trim_galore.txt
    """

}


/////////////////
// trim_galore //
/////////////////

process quality_filter {

	tag { "file: ${file_id}" }

	publishDir "${params.prepOut}", 
		mode: 'copy', 
		saveAs: { filename ->
			if( filename.indexOf(".fq") > 0 )
				return "$filename"
			else if( filename.indexOf("_trimming_report.txt") > 0)
				return "$filename"
			else if( filename.indexOf("_fastqc.zip") > 0)
				return "$filename"
			else if( filename.indexOf("_fastqc.html") > 0)
				return "$filename"
			else
				return null
		}

	input:
	set val(file_id), file(reads) from p_qualityFilter

	output:
	set val(file_id), 
	file("*1_val_1.fq"), 
	file("*2_val_2.fq"), 
	file("*1_unpaired_1.fq"), 
	file("*2_unpaired_2.fq")
	file "*_trimming_report.txt"
	file "*_fastqc.{zip,html}"

	"""
	trim_galore --phred33 --paired --illumina -q 25 --retain_unpaired --length 75 --max_n 1 ${reads} --fastqc --dont_gzip
	"""

}

workflow.onComplete {

    println "Completed at: $workflow.complete"
    println "Execution status: ${ workflow.success ? 'OK' : 'failed' }"

}

