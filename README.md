[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# ResPipe
###### Current version: 1.6.1 (20230117)
###### CARD version: 3.2.4 (20220912)

ResPipe paper ("The impact of sequencing depth on the inferred taxonomic composition and AMR gene content of metagenomic samples") is [published](https://environmentalmicrobiome.biomedcentral.com/articles/10.1186/s40793-019-0347-1).


## A. NEXTFLOW

As a Nextflow-based pipeline, you will need to install nextflow together with a fair number of dependencies (listed below). Please refer to Nextflow webpage for more information [https://www.nextflow.io/](https://www.nextflow.io/).


## B. What is ResPipe?

It is a workflow for interrogating metagenomes for Antimicrobial Resistance Genes (CARD-based), Insertion Sequences and Enterobactericeae Plasmids. The main difference between ResPipe and other pipelines is that for each AMR, we map metagenomics reads onto its unique region. This is a conservative approach; the reasoning behind this is to avoid overestimating AMR by mapping reads onto a conserved region(s). Please refer to our paper for more detailed explanations.

There are three main stages in **ResPipe**:

1. Quality filtering and adapter removal
2. Functional profiling against 
	- [CARD](https://card.mcmaster.ca/)
	- [ISfinder](https://www-is.biotoul.fr/)
	- [NCBI Enterobacter Plasmids database](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5426034/)
	- Also Thermus spike-in counts for "pseudo-quantitative" normalisation. This is if you spiked-in Thermus Thermophilus DNA in your samples.
3. Taxonomic profiling with KRAKEN2 (default) and BRACKEN (and CENTRIFUGE)
4. 16S rRNA extraction [optional]
5. Count number of 31 selected single copy genes [optional]

## C. Dependencies (tested versions)

* **nextflow** conda install -c bioconda nextflow
* **fastqc** (0.11.5) conda install -c bioconda fastqc
* **cutadapt** (1.15) conda install -c bioconda cutadapt
* **bbmap** (37.72) conda install -c bioconda bbmap
* **samtools** (1.6) conda install -c bioconda samtools
* **ncurses** (6.1) conda install -c conda-forge ncurses
* **bedtools** (2.27.0) conda install -c bioconda bedtools
* **TrimGalore** (0.5.0) conda install -c bioconda trim-galore
* **seqtk** (1.3) conda install -c bioconda seqtk
* **hmmer** (3.2) conda install -c bioconda hmmer
* **orfm** (0.7.1) conda install -c bioconda orfm
* **diamond** (0.9.22) conda install -c bioconda diamond
* **pandas** (0.19.2) conda install pandas
* **kraken2** (v2.0.8-beta) [https://github.com/DerrickWood/kraken2](https://github.com/DerrickWood/kraken2)
* **bracken** (1.0.0) [https://ccb.jhu.edu/software/bracken/](https://ccb.jhu.edu/software/bracken/)
* **centrifuge** (1.0.3-beta) [https://ccb.jhu.edu/software/centrifuge/manual.shtml](https://ccb.jhu.edu/software/centrifuge/manual.shtml)


## D. Prerequisites

* **Sequence data** in FASTQ or BAM (must be paired-end sequences).

* **A sample list**: this file needs at least two columns separated by a tab - file
identifiers (one for each pair of fastqs, and without _1.fastq.gz or _2.fastq.gz extension)
and sample names. N.B. Where two files come from the same sample, ResPipe will merge them together
using this list.

	### Example 1
		
	Assuming, you have three pairs of FASTQ files namely:
	1. WTCHG\_377463\_208107\_1.fastq.gz, WTCHG\_377463\_208107\_2.fastq.gz
	2. WTCHG\_377463\_209190\_1.fastq.gz, WTCHG\_377463\_209190\_2.fastq.gz
	3. WTCHG\_377463\_210178\_1.fastq.gz, WTCHG\_377463\_210178\_2.fastq.gz
	
	your sample list file will need to look like:

	```
	WTCHG_377463_208107	Cambodia_Faecal_A
	WTCHG_377463_209190	Cambodia_Faecal_A
	WTCHG_377463_210178	UK_Faecal_C_1_6_12_A_N_Y_T_H_I_N_G
	```
	
	In the above case, the first two sequence data will automatically be merged into a sample called "Cambodia_Faecal_A" sample.


	### Example 2
	
	Assuming you have three bam files namely Cambodia\_Faecal\_A.bam, Cambodia\_Faecal\_B.bam and UK\_Faecal\_A.bam:

	```
	Cambodia_Faecal_A	SAMPLE_Cambodia_Faecal_A
	Cambodia_Faecal_B	SAMPLE_Cambodia_Faecal_A
	UK_Faecal_A	        SAMPLE_UK_Faecal_A
	```

	Again, here Cambodia_Faecal_A.bam and Cambodia_Faecal_B.bam will be merged. 



## E. Running ResPipe

### 1. Prepping sequences

- By default, the following command produces a directory called "ResPipe_prep". If you want a different output directory, use ```--out OUTPUT_DIR```

	```
	ResPipe_prep.nf --in fastq_dir --readPattern "*_r{1,2}.fq.gz"
	```

- If you are starting with a bam file you can convert it to paired fastqs with the "--bam" flag

	```
	ResPipe_prep.nf --in sample_dir --bam
	```

### 2. Checking the integrity of your sample list file

- Check your listfile against fastq directory to ensure all data are intact with the following command. If there are no issues, then you are good to go!

    ```
    ResPipe_checkSampleList.py --in fastq_dir -l samplelist.tsv --fastq
    ```

### 3. Functional Profiling

- Run with the following:

    ```
    ResPipe_amr.nf --in ResPipe_prep
    ```

- For samples with Thermus spike-in: (currently supports [HB27](https://www.ncbi.nlm.nih.gov/nuccore/NC_005835) and [HB8](https://www.ncbi.nlm.nih.gov/nuccore/NC_006461) strain)

    ```
    ResPipe_amr.nf --in ResPipe_prep --func --thermus HB27
    ```

### 4. Taxonomic Profiling

- Run with the following:

	```
	ResPipe.nf --in ResPipe_prep --listfile samplelist.tsv --taxo --kraken_preload (or --kraken_ramdisk)
	```
	You have to specify either --kraken\_preload or --kraken\_ramdisk
	
- You can run with a database in specific location with the kraken\_db, braken\_distrib and/or the centrifuge\_abv parameters:

    ```
    ResPipe.nf --in ResPipe_prep --listfile samplelist.tsv --taxo --kraken_db /location/of/kraken/minikraken_20171101_8GB_dustmasked --braken_distrib /location/of/braken/minikraken_8GB_75mers_distrib.txt --centrifuge_abv /location/of/centrifuge/p_compressed+h+v
    ```

- You can also choose run only Centrifuge, or Kraken with --krakenOnly or --centrifugeOnly:

    ```
    Respipe.nf --in Respipe_prep --listfile samplelist.tsv --taxo --krakenOnly --kraken_preload (or --kraken_ramdisk)    
    ```
    
    ```
    Respipe.nf --in Respipe_prep --listfile samplelist.tsv --taxo --centrifugeOnly

    ```

### 5. 16S rRNA Extraction

- ResPipe will extract 16S using HMMER3:

	```
	ResPipe.nf --in ResPipe_prep --listfile samplelist.tsv --extract16S
	```

### 6. 31 Single Copy Gene counting

- ResPipe will count how many 31 selected single copy genes are present in your samples:

	```
	ResPipe.nf --in ResPipe_prep --listfile samplelist.tsv --countSCG
	```

## F. Further Running Information

### 1. Running with a test dataset

- Test data is provided in ```test_data``` directory.
- Run with the following commands:

    ```
    ResPipe_prep.nf --in bams --bam
    ResPipe_amr.nf --in ResPipe_prep
    ResPipe.nf --in ResPipe_prep --listfile samplelist.tsv --taxo --kraken_preload
    ```

### 2. Running in alternative environments

- There are some profiles provided within the nextflow.config file corresponding to some common execution environments (e.g. a SLURM cluster). These set variables such as the Database locations and enable execution options such as the use of containers (as well as the location of the associated images).

- The example below runs the ResPipe_prep pipeline in a SLURM environment. It also enables the trace, report and timeline functionality of nextflow explictly if not enabled in the profile. In addition if ran on a previously failed run this will resume using previously completed steps.

    ```
    ResPipe_prep.nf --in fastq_dir -profile slurm -with-trace -with-report -with-timeline -resume
    ```
