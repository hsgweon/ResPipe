FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
  build-essential \
  gcc \
  make \
  python \
  python3-dev \
  python3-setuptools \
  python3-pip \
  wget \
  bzip2 \
  curl \
  openjdk-8-jre \
  openjdk-8-jdk \
  git \
  hmmer \
    && apt-get clean && rm -rf /var/lib/apt/lists/* 

RUN curl -sSL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /tmp/miniconda.sh \
    && bash /tmp/miniconda.sh -bfp /usr/local \
    && rm -rf /tmp/miniconda.sh \
    && conda install -y python=3 \
    && conda update conda \
    && conda clean --all --yes

ENV PATH /opt/conda/bin:$PATH

RUN conda install -y -c bioconda perl-threaded  cutadapt fastqc pandas samtools bedtools 
#    && conda install -y -c bioconda cutadapt \
#    && conda install -y -c bioconda fastqc \
#    && conda install -y pandas \
#    && conda install -y -c bioconda samtools \
#    && conda install -y -c bioconda bedtools

RUN pip install multiqc
RUN curl -fsSL https://github.com/FelixKrueger/TrimGalore/archive/0.4.3.tar.gz -o trim_galore.tar.gz \
    && tar -xvzf trim_galore.tar.gz \
    && mv TrimGalore-0.4.3/trim_galore /usr/bin \
    && rm -rf trim_galore.tar.gz TrimGalore-0.4.3

ENV URL http://downloads.sourceforge.net/project/bbmap/BBMap_37.72.tar.gz
ENV BUILD_DIR /usr/local/bbmap
ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk-amd64
RUN wget -qO - $URL \
    | tar -C $(dirname $BUILD_DIR) -zxf - \
    && cd $BUILD_DIR/jni \
    && make -f $BUILD_DIR/jni/makefile.linux

ENV PATH="$BUILD_DIR:${PATH}"
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# install NCBI BLAST for Centrifuge
RUN wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.7.1/ncbi-blast-2.7.1+-x64-linux.tar.gz \
    && tar -xzf ncbi-blast-2.7.1+-x64-linux.tar.gz \
    && mv ncbi-blast-2.7.1+/bin/* /usr/bin \
    && rm -rf ncbi-blast-2.7.1+-x64-linux.tar.gz ncbi-blast-2.7.1+

# install Centrifuge
RUN git clone https://github.com/infphilo/centrifuge \
    && cd centrifuge \
    && make \
    && make install prefix=/usr/local \
    && cd ../ && rm -rf centrifuge

# install kraken2
RUN git clone https://github.com/DerrickWood/kraken2.git \
    && cd kraken2 \
    && ./install_kraken2.sh kraken_scripts \
    && mv kraken_scripts/* /usr/bin \
    && cd .. && rm -rf kraken2

# install orfm
RUN wget https://github.com/wwood/OrfM/releases/download/v0.7.1/orfm-0.7.1_Linux_x86_64.tar.gz \
    && tar -xzf orfm-0.7.1_Linux_x86_64.tar.gz \
    && mv orfm-0.7.1_Linux_x86_64/* /usr/bin \
    && rm -rf orfm-0.7.1*

# install diamond
RUN wget http://github.com/bbuchfink/diamond/releases/download/v0.9.26/diamond-linux64.tar.gz \
    && tar -xzf diamond-linux64.tar.gz \
    && mv diamond /usr/bin \
    && rm -rf diamond*

#install seqtk
RUN git clone https://github.com/lh3/seqtk.git \
    && cd seqtk \
    && make \
    && mv seqtk /usr/bin \
    && cd ../ && rm -rf seqtk

# install bracken
RUN git clone https://github.com/jenniferlu717/Bracken.git \
    && cd Bracken/src && make

ENV PATH="/Bracken:/Bracken/src:${PATH}"
